'use strict'

// Конфигурация PostCSS
module.exports = ({ env }) => ({
  plugins: {
    'autoprefixer': {},
    'cssnano': (env === 'production') ? {} : false,
    'css-mquery-packer': {}
  }
})
