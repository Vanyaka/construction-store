'use strict'

// Пакеты
const dotenv = require('dotenv').config()
const minimist = require('minimist')

// Значения
const args = minimist(process.argv.slice(2)) 
const name = args.name || process.env.name || 'КрымКрепёж'
const host = args.host || process.env.host || '127.0.0.1'
const port = args.port || process.env.port || 8080
const db_name = args.db_name || process.env.db_name || 'cs'
const db_host = args.db_host || process.env.db_host || 'localhost'
const db_port = args.db_port || process.env.db_port || 27017
const db_user = args.db_user || process.env.db_user
const db_password = args.db_password || process.env.db_password
const secret = args.secret || process.env.secret || 'authSecretCS'
const admin_email = args.admin_email || process.env.admin_email || 'admin@mail.ru'
const admin_password = args.admin_password || process.env.admin_password || 'Admin#1'

// Конфигурации
module.exports = {
  name: name,
  host: host,
  port: port,
  mongodb: { // Данные для подключения БД
    uri: `mongodb://${(db_user && db_password) ? `${db_user}:${db_password}@` : ''}${db_host}:${db_port}/${db_name}`,
    options: {
      useCreateIndex: true,
      useNewUrlParser: true,
      useFindAndModify: false,
      useUnifiedTopology: true
    },
    session: {
      secret: secret,
      resave: false,
      saveUninitialized: false,
      cookie: {
        maxAge: (1000 * 60 * 60 * 24) * 30 // 30 дней
      }
    }
  },
  defaults: { // Значения "По умолчанию"
    admin: { // Данные для инициализации админа
      email: admin_email,
      password: admin_password
    },
    onPage: 25, // Кол-во элементов на странице
    display: 'table' // Формат отображения
  },
  admin: { // Данные для инициализации админа
    email: admin_email,
    password: admin_password
  },
  messages: { // Сообщения
    statuses: { // HTTP статусы
      400: 'Неверный запрос', // BadRequest
      403: 'Доступ запрещён', // Forbidden
      404: 'По вашему запросу ничего не найдено', // NotFound
      405: 'Данный метод не поддерживается', // MethodNotAllowed

      500: 'Ошибка на стороне сервера' // InternalServerError
    }
  },
  regexes: { // Регулярки
    ObjectID: /^[a-f\d]{24}$/i, // Шаблон ObjectID
    specialSymbols: /[-[\]{}()*+?.,\\^$|#\s]/g, // Специальные символы регулярных выражений
    URL: /^(((ftp|http|https):\/\/)|(\/)|(..\/))(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/ // URL
  }
}
