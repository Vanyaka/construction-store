'use strict'

/**
 * Парсинг cookies
 * @description разбивает строку cookies на пары "название:значение" (если встречается JSON-строка, то она парсится)
 * @returns объект со структурированными cookies
 */
export function parse() {
  let result = {}

  if (!document.cookie.length) return result

  for (const item of document.cookie.split(';')) {
    let [ name, value ] = item.split('=')

    name = decodeURIComponent(name).trim()
    value = decodeURIComponent(value).trim()

    result[name] = (isJSON(value)) ? JSON.parse(value) : value
  }

  return result
}

/**
 * Получение cookie
 * @description возвращает значение cookie с указанным именем
 * @param {String} name - имя записи
 * @returns значение cookie
 */
export function get(name) {
  return parse()[name]
}

/**
 * Запись cookie
 * @description записывает в cookies переданные данные
 * @param {String} name - название
 * @param {*} value - значение (преобразовывается в строку)
 * @param {Object} options - опции
 */
export function set(name, value, options) {
  let cookieData = `${encodeURIComponent(name)}=${encodeURIComponent((isObject(value) ? JSON.stringify(value) : value))}`

  for (const key in options) {
    cookieData += `; ${key}=${options[key]}`
  }

  document.cookie = cookieData
}

/**
 * Запись cookies
 * @param {Object[]} data - массив с данными для записи
 * @param {String} data[].name - название
 * @param {*} data[].value - значение (преобразовывается в строку)
 * @param {Object} data[].options - опции
 */
export function setMany(data) {
  for (const item of data) {
    set(item.name, item.value, item.options)
  }
}

/**
 * Удаление cookie
 * @description устанавливает cookie с указанным именем отрицательный срок жизни
 * @param {String} name - имя
 */
export function remove(name) {
  set(name, '', { 'max-age': -1 })
}

/**
 * Удаление cookies
 * @description устанавливает cookies с указанными именами отрицательный срок жизни
 * @param {String[]} names - массив имён
 */
export function removeMany(names) {
  for (const name of names) {
    remove(name)
  }
}

/** 
 * Очистка cookies
 * @description устанавливает всем cookies отрицательный срок жизни
*/
export function clear() {
  for (const key in parse()) {
    remove(key)
  }
}

/**
 * Проверка на объект
 * @description проверяет, является ли переданное значение настоящим объектом (массивы, даты, null и т.п. не считаются объектами)
 * @param {*} value - значение
 * @returns true | false
 */
function isObject(value) {
  if (typeof value != 'object' || value === null || Array.isArray(value) || value.toString() != '[object Object]') return false
  return true
}

/**
 * Проверка на JSON-строку
 * @description проверяет, является ли переданное значение JSON-строкой
 * @param {*} value - значение
 * @returns true | false
 */
function isJSON(value) {
  try {
    JSON.parse(value)
    return true
  } catch (err) {
    return false
  }
}
