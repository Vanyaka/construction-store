'use strict'

// Пакеты
import Swal from 'sweetalert2'
import { getIcon } from './functions'

// Инициализация кнопок удаления элемента
export function deleteOne() {
  document.querySelectorAll('#delete').forEach(button => {
    button.addEventListener('click', async () => {
      const response = await fetch(location.href, {
        method: 'DELETE',
        headers: { 'Accept': 'application/json' }
      })

      try {
        const result = await response.json()

        if (response.ok) {
          location.href = result.url
        } else {
          Swal.fire({ 
            icon: getIcon(result.status), 
            title: result.msg,
            customClass: { confirmButton: 'btn btn-primary' }
          })
        }
      } catch (err) {
        Swal.fire({ 
          icon: 'error', 
          title: 'Не удалось расшифровать ответ сервера',
          customClass: { confirmButton: 'btn btn-primary' }
        })
      }
    })
  })
}

// Инициализация кнопок множественного удаления
export function deleteMany() {
  document.querySelectorAll('#deleteMany').forEach(button => {
    button.addEventListener('click', async () => {
      const mainCheck = document.querySelector('.table th .form-check-input')

      if (!!mainCheck) {
        const response = await fetch(location.href, {
          method: 'DELETE',
          headers: { 
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({ IDs: mainCheck.value.split(',') })
        })

        try {
          const result = await response.json()
  
          if (response.ok) {
            location.href = result.url
          } else {
            Swal.fire({ 
              icon: getIcon(result.status), 
              title: result.msg,
              customClass: { confirmButton: 'btn btn-primary' }
            })
          }
        } catch (err) {
          Swal.fire({ 
            icon: 'error', 
            title: 'Не удалось расшифровать ответ сервера',
            customClass: { confirmButton: 'btn btn-primary' }
          })
        }
      } else {
        Swal.fire({ 
          icon: 'warning', 
          title: 'Не удалось обнаружить выделенных элементов',
          customClass: { confirmButton: 'btn btn-primary' }
        })
      }
    })
  })
}
