'use strict'

// Пакеты
import { Alert } from 'bootstrap'
import * as qs from 'qs'
import { getAncestor } from './functions'

const pages = {
  categories: {
    init: async () => {
      try {
        return {
          categories: await fetchShell('/api/getCategories', { method: 'GET', headers: { 'Accept': 'application/json' } }, 'json')
        }
      } catch (err) {
        throw err
      }
    },
    fields: {
      name: {
        title: 'Название',
        template: ({ count, value }) => {
          return `
          <div class="col-sm-12">
            <input name="name" class="form-control form-control-sm" type="text"${(!!value) ? ` value="${value.text}"` : ''}>
          </div>
          <div class="col-sm-12 row">
            <div class="form-check col-auto">
            <input class="form-check-input" type="radio" name="nameRadio${count}" id="nameRadio${count}-1" value="contains"${(!value) ? ' checked' : (value.position == 'contains') ? ' checked' : ''}>
            <label class="form-check-label" for="nameRadio${count}-1">Содержит</label>
            </div>
            <div class="form-check col-auto">
              <input class="form-check-input" type="radio" name="nameRadio${count}" id="nameRadio${count}-2" value="start"${(!!value && value.position == 'start') ? ' checked' : ''}>
              <label class="form-check-label" for="nameRadio${count}-2">Начинается</label>
            </div>
            <div class="form-check col-auto">
              <input class="form-check-input" type="radio" name="nameRadio${count}" id="nameRadio${count}-3" value="end"${(!!value && value.position == 'end') ? ' checked' : ''}>
              <label class="form-check-label" for="nameRadio${count}-3">Заканчивается</label>
            </div>
          </div>
          `
        }
      },
      parent: {
        title: 'Родительская категория',
        template: ({ categories, value }) => {
          if (!!categories.length) {
            let dataList = ''
    
            for (const item of categories) {
              dataList += `<option value="${item.name}">`
            }
    
            return `
            <div class="col-sm">
              <input name="parent" class="form-control form-control-sm" list="parentList" placeholder="Начните вводить для поиска..."${(!!value) ? ` value="${categories.find(item => item._id == value).name}"` : ''}>
              <datalist id="parentList">${dataList}</datalist>
            </div>
            `
          } else {
            return `
            <div class="col-sm">
              <input name="parent" class="form-control form-control-sm" list="datalistOptions" placeholder="Название родительской категории"${(!!value) ? ` value="${categories.find(item => item._id == value).name}"` : ''}>
            </div>
            `
          }
        }
      }
    }
  },
  products: {
    init: async () => {
      try {
        return {
          categories: await fetchShell('/api/getCategories', { method: 'GET', headers: { 'Accept': 'application/json' } }, 'json')
        }
      } catch (err) {
        throw err
      }
    },
    fields: {
      name: {
        title: 'Название',
        template: ({ count, value }) => {
          return `
          <div class="col-sm-12">
            <input name="name" class="form-control form-control-sm" type="text"${(!!value) ? ` value="${value.text}"` : ''}>
          </div>
          <div class="col-sm-12 row">
            <div class="form-check col-auto">
            <input class="form-check-input" type="radio" name="nameRadio${count}" id="nameRadio${count}-1" value="contains"${(!value) ? ' checked' : (value.position == 'contains') ? ' checked' : ''}>
            <label class="form-check-label" for="nameRadio${count}-1">Содержит</label>
            </div>
            <div class="form-check col-auto">
              <input class="form-check-input" type="radio" name="nameRadio${count}" id="nameRadio${count}-2" value="start"${(!!value && value.position == 'start') ? ' checked' : ''}>
              <label class="form-check-label" for="nameRadio${count}-2">Начинается</label>
            </div>
            <div class="form-check col-auto">
              <input class="form-check-input" type="radio" name="nameRadio${count}" id="nameRadio${count}-3" value="end"${(!!value && value.position == 'end') ? ' checked' : ''}>
              <label class="form-check-label" for="nameRadio${count}-3">Заканчивается</label>
            </div>
          </div>
          `
        }
      },
      category: {
        title: 'Категория',
        template: ({ categories, value }) => {
          if (!!categories.length) {
            let dataList = ''
    
            for (const item of categories) {
              dataList += `<option value="${item.name}">`
            }
    
            return `
            <div class="col-sm">
              <input name="category" class="form-control form-control-sm" list="categoryList" placeholder="Начните вводить для поиска..."${(!!value) ? ` value="${categories.find(item => item._id == value).name}"` : ''}>
              <datalist id="categoryList">${dataList}</datalist>
            </div>
            `
          } else {
            return `
            <div class="col-sm">
              <input name="category" class="form-control form-control-sm" list="datalistOptions" placeholder="Название категории"${(!!value) ? ` value="${categories.find(item => item._id == value).name}"` : ''}>
            </div>
            `
          }
        }
      }
    }
  },
  articles: {
    init: async () => {
      try {
        const tmp = await Promise.all([
          fetchShell('/api/getMeasureNames', { method: 'GET', headers: { 'Accept': 'application/json' } }, 'json'),
          fetchShell('/api/getProducts', { method: 'GET', headers: { 'Accept': 'application/json' } }, 'json')
        ])

        return {
          measureNames: tmp[0],
          products: tmp[1]
        }
      } catch (err) {
        throw err
      }
    },
    fields: {
      product: {
        title: 'Товар',
        template: ({ products, value }) => {
          if (!!products.length) {
            let dataList = ''
    
            for (const item of products) {
              dataList += `<option value="${item.name}">`
            }
    
            return `
            <div class="col-sm">
              <input name="product" class="form-control form-control-sm" list="productList" placeholder="Начните вводить для поиска..."${(!!value) ? ` value="${products.find(item => item._id == value).name}"` : ''}>
              <datalist id="productList">${dataList}</datalist>
            </div>
            `
          } else {
            return `
            <div class="col-sm">
              <input name="product" class="form-control form-control-sm" list="datalistOptions" placeholder="Название товара"${(!!value) ? ` value="${products.find(item => item._id == value).name}"` : ''}>
            </div>
            `
          }
        }
      },
      code: {
        title: 'Код',
        template: ({ value }) => {
          return `
          <div class="col-sm">
            <input name="min" type="number" class="form-control form-control-sm" placeholder="Min" min="0"${(!!value) ? ` value="${value.min}"` : ''}>
          </div>
          <div class="col-sm">
            <input name="max" type="number" class="form-control form-control-sm" placeholder="Max" min="0"${(!!value) ? ` value="${value.max}"` : ''}>
          </div>
          `
        }
      },
      name: {
        title: 'Название',
        template: ({ count, value }) => {
          return `
          <div class="col-sm-12">
            <input name="name" class="form-control form-control-sm" type="text"${(!!value) ? ` value="${value.text}"` : ''}>
          </div>
          <div class="col-sm-12 row">
            <div class="form-check col-auto">
            <input class="form-check-input" type="radio" name="nameRadio${count}" id="nameRadio${count}-1" value="contains"${(!value) ? ' checked' : (value.position == 'contains') ? ' checked' : ''}>
            <label class="form-check-label" for="nameRadio${count}-1">Содержит</label>
            </div>
            <div class="form-check col-auto">
              <input class="form-check-input" type="radio" name="nameRadio${count}" id="nameRadio${count}-2" value="start"${(!!value && value.position == 'start') ? ' checked' : ''}>
              <label class="form-check-label" for="nameRadio${count}-2">Начинается</label>
            </div>
            <div class="form-check col-auto">
              <input class="form-check-input" type="radio" name="nameRadio${count}" id="nameRadio${count}-3" value="end"${(!!value && value.position == 'end') ? ' checked' : ''}>
              <label class="form-check-label" for="nameRadio${count}-3">Заканчивается</label>
            </div>
          </div>
          `
        }
      },
      measureName: {
        title: 'Ед. изм.',
        template: ({ measureNames, value }) => {
          if (!!measureNames.length) {
            let dataList = ''
      
            for (const item of measureNames) {
              dataList += `<option value="${item}">`
            }
      
            return `
            <div class="col-sm">
              <input name="measureName" class="form-control form-control-sm" list="measureNameList" placeholder="Начните вводить для поиска..."${(!!value) ? ` value="${value}"` : ''}>
              <datalist id="measureNameList">${dataList}</datalist>
            </div>
            `
          } else {
            return `
            <div class="col-sm">
              <input name="measureName" class="form-control form-control-sm" type="text" placeholder="Еденицы измерения"${(!!value) ? ` value="${value}"` : ''}>
            </div>
            `
          }
        }
      },
      price: {
        title: 'Розничная цена',
        template: ({ value }) => {
          return `
          <div class="col-sm">
            <input name="min" type="number" class="form-control form-control-sm" placeholder="Min" min="0" step="any"${(!!value) ? ` value="${value.min}"` : ''}>
          </div>
          <div class="col-sm">
            <input name="max" type="number" class="form-control form-control-sm" placeholder="Max" min="0" step="any"${(!!value) ? ` value="${value.max}"` : ''}>
          </div>
          `
        }
      },
      costPrice: {
        title: 'Оптовая цена',
        template: ({ value }) => {
          return `
          <div class="col-sm">
            <input name="min" type="number" class="form-control form-control-sm" placeholder="Min" min="0" step="any"${(!!value) ? ` value="${value.min}"` : ''}>
          </div>
          <div class="col-sm">
            <input name="max" type="number" class="form-control form-control-sm" placeholder="Max" min="0" step="any"${(!!value) ? ` value="${value.max}"` : ''}>
          </div>
          `
        }
      },
      quantity: {
        title: 'Количество',
        template: ({ value }) => {
          return `
          <div class="col-sm">
            <input name="min" type="number" class="form-control form-control-sm" placeholder="Min"${(!!value) ? ` value="${value.min}"` : ''}>
          </div>
          <div class="col-sm">
            <input name="max" type="number" class="form-control form-control-sm" placeholder="Max"${(!!value) ? ` value="${value.max}"` : ''}>
          </div>
          `
        }
      },
      active: {
        title: 'Отображение',
        template: ({ count, value }) => {
          return `
          <div class="form-check col-auto">
            <input class="form-check-input" type="radio" name="activeRadio${count}" id="activeRadio${count}-1" value="active"${(!value) ? ' checked' : (value == 'active') ? ' checked' : ''}>
            <label class="form-check-label" for="activeRadio${count}-1">Активные</label>
          </div>
          <div class="form-check col-auto">
            <input class="form-check-input" type="radio" name="activeRadio${count}" id="activeRadio${count}-2" value="disabled"${(!!value && value == 'disabled') ? ' checked' : ''}>
            <label class="form-check-label" for="activeRadio${count}-2">Отключенные</label>
          </div>
          `
        }
      }
    }
  }
}

// Инициализация фильтрации
export default async () => {
  const displayType = document.querySelector('.btn-group.display .active')
  const page = pages[location.pathname.split('/').pop()]

  if (!!page && (!displayType || displayType.value == 'table')) {
    const params = qs.parse(location.href.split('?')[1])
    const data = await page.init()
  
    let filterCounter = 0
  
    // Кнопка "+ Фильтр"
    document.querySelectorAll('[name="addFilter"]').forEach(item => {
      item.addEventListener('click', addFilter)
    })
  
    // Кнопка "Сбросить"
    document.querySelectorAll('[name="resetFilters"]').forEach(item => {
      item.addEventListener('click', () => {
        document.querySelectorAll('.filters .alert').forEach(item => {
          (new Alert(item)).close()
  
          const query = qs.stringify(deleteProperties(qs.parse(location.href.split('?')[1]), Object.keys(page.fields).concat([ 'start' ])))
          location.href = `${location.pathname}${(!!query) ? '?' : ''}${query}`
        })
      })
    })
  
    // Кнопка "Применить"
    document.querySelectorAll('[name="applyFilters"]').forEach(item => {
      item.addEventListener('click', async () => {
        let filterData = {}
  
        document.querySelectorAll('.filters .filter').forEach(item => {
          const fieldName = item.querySelector('[name="field"]').value.trim()
          
          if (!!fieldName) {
            if (!filterData[fieldName]) filterData[fieldName] = new Set
  
            if (fieldName == 'product') {
              const field = item.querySelector(`input[name="${fieldName}"]`)
              if (checkFields(field)) {
                for (const product of data.products) {
                  if (product.name == field.value) filterData[fieldName].add(product._id)
                }
              }
            } 
  
            if ([ 'category', 'parent' ].includes(fieldName)) {
              const field = item.querySelector(`input[name="${fieldName}"]`)
              if (checkFields(field)) {
                for (const category of data.categories) {
                  if (category.name == field.value) filterData[fieldName].add(category._id)
                }
              }
            }
  
            if (fieldName == 'measureName') {
              const field = item.querySelector(`input[name="${fieldName}"]`)
              if (checkFields(field)) filterData[fieldName].add(field.value)
            } 
  
            if ([ 'code', 'price', 'costPrice', 'quantity' ].includes(fieldName)) {
              const [ min, max ] = [ item.querySelector('input[name="min"]'), item.querySelector('input[name="max"]') ]
              let tmp = {}
              if (checkFields(min)) tmp.min = min.value
              if (checkFields(max)) tmp.max = max.value
              if (!!Object.keys(tmp).length) filterData[fieldName].add(tmp)
            }
  
            if (fieldName == 'name') {
              const [ text, position ] = [ item.querySelector('input[name="name"]'), item.querySelector('input[type="radio"]:checked') ]
              if (checkFields([ text, position ])) filterData[fieldName].add({ 
                text: text.value, 
                position: position.value 
              })
            } 
       
            if (fieldName == 'active') {
              const field = item.querySelector('input[type="radio"]:checked')
              if (checkFields(field)) filterData[fieldName].add(field.value)
            }
          } 
        })
  
        for (const key in filterData) {
          filterData[key] = [...filterData[key]]
        }
  
        const query = qs.stringify(Object.assign(deleteProperties(qs.parse(location.href.split('?')[1]), Object.keys(page.fields).concat([ 'start' ])), filterData))
        location.href = `${location.pathname}${(!!query) ? '?' : ''}${query}`
      })
    })
  
    // Построение фильтров из url
    for (const key in params) {
      if (page.fields.hasOwnProperty(key)) {
        for (const item of params[key]) addFilter(null, key, item)
      }
    }

    // Добавление фильтра
    function addFilter(event, fieldName, value) {
      const filters = document.querySelector('.filters')
      const filter_buttons = document.querySelector('.filter_buttons')
      const div = document.createElement('div')
  
      div.className = 'alert alert-light alert-dismissible fade show border filter'
  
      div.innerHTML = `
      <div class="row g-3">
        <div class="col-sm-4">
          <select name="field" class="form-select form-select-sm filter-field">
            <option value="" selected>Выберите поле</option>
            ${getOptions(page.fields, fieldName)}
          </select>
        </div>
        <div class="col-sm row gy-2 gx-3 filter-options">
        ${(!!value) ? page.fields[fieldName].template(Object.assign({ count: ++filterCounter, value }, data))  : ''}
        </div>
      </div>
      <button type="button" class="close" data-dismiss="alert">
        <span aria-hidden="true">&times;</span>
      </button>
      `
  
      div.querySelector('.filter-field').addEventListener('change', (event) => {
        getAncestor(event.target, { class: 'row' }).querySelector('.filter-options').innerHTML = (!!event.target.value) ? page.fields[event.target.value].template(Object.assign({ count: ++filterCounter }, data)) : ''
      })
  
      div.addEventListener('closed.bs.alert', () => {
        if (!filters.querySelectorAll('.filters .filter').length) {
          filter_buttons.classList.remove('show')
  
          const query = qs.stringify(deleteProperties(qs.parse(location.href.split('?')[1]), Object.keys(page.fields).concat([ 'start' ])))
          location.href = `${location.pathname}${(!!query) ? '?' : ''}${query}`
        } 
      })
  
      filters.append(div)
  
      if (!filter_buttons.classList.contains('show')) filter_buttons.classList.add('show')
    }
  }
}

// Проверка полей
function checkFields(data) {
  if (Array.isArray(data)) {
    for (const item of data) {
      if (!item || !item.value) return false
    }
  } else {
    if (!data || !data.value) return false
  }

  return true
}

// Обёртка для fetch
async function fetchShell(url, options, type) {
  try {
    const response = await fetch(url, options)

    if (!response.ok) throw 'Нет данных' 

    if (type == 'json') return await response.json()

    return response
  } catch (err) {
    return []
  }
}

// Удаление параметров
function deleteProperties(obj, properties) {
  for (const key of properties) {
    delete obj[key]
  } 

  return obj
}

// Получение списка опций
function getOptions(data, selectKey) {
  let result = ''

  for (const key in data) {
    result += `<option value="${key}"${(key == selectKey) ? ' selected' : ''}>${data[key].title}</option>`
  }

  return result
}
