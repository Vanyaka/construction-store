'use strict'

// Пакеты
import { getAncestor } from './functions'

// Инициализация интерактивного плейсхолдера для поля загрузки файлов
export default () => {
  document.querySelectorAll('.form-file .form-file-text[data-placeholder]').forEach(fileText => {
    const formFile = getAncestor(fileText, { class: 'form-file' })
    const fileInput = formFile.querySelector('.form-file-input')

    // Обработка изменения содержимого поля
    fileInput.addEventListener('change', event => {
      const files = event.target.files
      let names = []

      for (let key in files) {
        if (files.hasOwnProperty(key)) names.push(files[key].name)
      }

      if (!!names.length) {
        fileText.textContent = names.join(', ')
      } else {
        fileText.textContent = fileText.dataset['placeholder']
      }
    })
  })
}
