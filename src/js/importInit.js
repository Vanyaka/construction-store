'use strict'

import Swal from 'sweetalert2'
import { getIcon } from './functions'

// Инициализация кнопок импорта
export default function importInit() {
  const addBtn = document.querySelector('#add')
  const updateBtn = document.querySelector('#update')
  const fileInput = document.querySelector('input[name="import"]')
  const spinner = getSpinner()
  
  if (!!addBtn) addBtn.addEventListener('click', async () => {
    const formData = new FormData()
    const file = fileInput.files[0]

    if (!!file) {
      addBtn.prepend(spinner)
      setDisabled([ addBtn, updateBtn, fileInput ], true)

      formData.append('data', file)

      const response = await fetch(location.href, {
        method: 'POST',
        headers: { 'Accept': 'application/json' },
        body: formData
      })

      try {
        const result = await response.json()

        if (response.ok) {
          Swal.fire({ 
            icon: 'success', 
            title: 'Добавление завершено!',
            html: `Добавлено: ${result.add}<br>Неудалось добавить: ${result.fails.length}`,
            customClass: { confirmButton: 'btn btn-primary' }
          })
        } else {
          Swal.fire({ 
            icon: getIcon(result.status), 
            title: result.msg,
            customClass: { confirmButton: 'btn btn-primary' }
          })
        }
      } catch (err) {
        Swal.fire({ 
          icon: 'error', 
          title: 'Не удалось расшифровать ответ сервера',
          customClass: { confirmButton: 'btn btn-primary' }
        })
      } finally {
        spinner.remove()
        setDisabled([ addBtn, updateBtn, fileInput ], false)
      }
      
    } else {
      Swal.fire({ 
        icon: 'warning', 
        title: 'Файл не выбран',
        customClass: { confirmButton: 'btn btn-primary' }
      })
    }
  })

  if (!!updateBtn) updateBtn.addEventListener('click', async () => {
    const formData = new FormData()
    const file = fileInput.files[0]

    if (!!file) {
      updateBtn.prepend(spinner)
      setDisabled([ addBtn, updateBtn, fileInput ], true)

      formData.append('data', file)

      const response = await fetch(location.href, {
        method: 'PATCH',
        headers: { 'Accept': 'application/json' },
        body: formData
      })

      try {
        const result = await response.json()

        if (response.ok) {
          Swal.fire({ 
            icon: 'success', 
            title: 'Обновление завершено!',
            html: `Добавлено: ${result.add}<br>Обновлено: ${result.update}<br>Скрыто: ${result.hide}`,
            customClass: { confirmButton: 'btn btn-primary' }
          })
        } else {
          Swal.fire({ 
            icon: getIcon(result.status), 
            title: result.msg,
            customClass: { confirmButton: 'btn btn-primary' }
          })
        }
      } catch (err) {
        Swal.fire({ 
          icon: 'error', 
          title: 'Не удалось расшифровать ответ сервера',
          customClass: { confirmButton: 'btn btn-primary' }
        })
      } finally {
        spinner.remove()
        setDisabled([ addBtn, updateBtn, fileInput ], false)
      }
      
    } else {
      Swal.fire({ 
        icon: 'warning', 
        title: 'Файл не выбран',
        customClass: { confirmButton: 'btn btn-primary' }
      })
    }
  })
}

function setDisabled(elements, value) {
  for (const item of elements) {
    item.disabled = value
  }
}

function getSpinner() {
  const span = document.createElement('span')

  span.className = 'spinner-border spinner-border-sm'
  span.setAttribute('role', 'status')
  span.setAttribute('aria-hidden', 'true')

  return span
}
