'use strict'

// Инициализация поля множественного выбора
export default () => {
  const select = document.querySelector('.select-many')

  if (!!select) {
    const input = select.querySelector('.input')
    const labels = select.querySelector('.labels')
    const options = select.querySelector('.options')
    const search = options.querySelector('.search')

    // Обработчик клика по селекту
    select.addEventListener('click', event => {
      const target = event.target

      if (!!target.classList.contains('select-many') || !!target.classList.contains('labels')) {
        if (!options.classList.contains('show')) {
          options.classList.add('show')
          if (!!search) search.focus()
          document.addEventListener('click', clickOutsideHandler)
        } else {
          options.classList.remove('show')
          document.removeEventListener('click', clickOutsideHandler)
        }
      } else if (!!target.classList.contains('option') && !target.classList.contains('selected')) {
        let value = (!!input.value.length) ? input.value.split(',') : []
        value.push(target.dataset['id'])
        input.value = value
        target.classList.add('selected')
        options.classList.remove('show')
        labels.appendChild(createLabel(target.dataset['id'], target.textContent.trim(), input, labels, options))
        document.removeEventListener('click', clickOutsideHandler)
      }
    })

    // Обработка нажатия клавиш внутри поля поиска
    if (!!search) {
      search.addEventListener('keyup', event => {
        for (const option of options.querySelectorAll('.option')) {
          if (!(new RegExp(`${event.target.value.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&')}`, 'i')).test(option.textContent)) {
            option.classList.add('hide')
          } else {
            option.classList.remove('hide')
          }
        }
      })
    }

    // Обрботка начальных значений
    if (!!input.value.trim().length) {
      for (const ID of input.value.split(',')) {
        const option = options.querySelector(`.option[data-id="${ID}"]`)

        if (!!option) {
          option.classList.add('selected')
          labels.appendChild(createLabel(option.dataset['id'], option.textContent.trim(), input, labels, options))
        }
      }
    }
  }
}

// Обработчик клика вне селекта
function clickOutsideHandler (event) {
  const select = document.querySelector('.select-many')
  
  if (!select.contains(event.target)) {
    select.querySelector('.options').classList.remove('show')
    document.removeEventListener('click', clickOutsideHandler)
  }
}

// Создание ярлыка
function createLabel (ID, text, input, labels, options) {
  const span = document.createElement('span')
  span.className = "label badge bg-secondary"
  span.setAttribute('data-id', ID)
  span.innerHTML = `
  ${text}
  <button type="button" class="close">
    <span title="Удалить">&times;</span>
  </button>
  `

  // Обработчик нажатия кнопки "x"
  const close = span.querySelector('.close')
  close.addEventListener('click', () => {
    input.value = input.value.replace(new RegExp(`(\\b,${ID}\\b)|(\\b${ID},\\b)|(\\b${ID}\\b)`, 'g'), '')
    labels.removeChild(close.parentNode)
    options.querySelector(`.options .option[data-id="${ID}"]`).classList.remove('selected')
  })

  return span
}
