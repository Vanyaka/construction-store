'use strict'

// Инициализация кнопок авторизации
export default function signInit() {
  const signinBtn = document.querySelector('.form-sign #signin')
  const signupBtn = document.querySelector('.form-sign #signup')

  if (!!signinBtn) signinBtn.addEventListener('click', async () => {
    const span = document.createElement('span')
    span.className = 'spinner-border spinner-border-sm'
    span.setAttribute('role', 'status')
    span.setAttribute('aria-hidden', 'true')

    document.querySelectorAll('input, button').forEach(item => { item.disabled = true })

    signinBtn.prepend(span)
  })

  if (!!signupBtn) signupBtn.addEventListener('click', async () => {
    const span = document.createElement('span')
    span.className = 'spinner-border spinner-border-sm'
    span.setAttribute('role', 'status')
    span.setAttribute('aria-hidden', 'true')

    document.querySelectorAll('input, button').forEach(item => { item.disabled = true })

    signupBtn.prepend(span)
  })
}
