'use strict'

// Пакеты
import Swal from 'sweetalert2'

// Инициализация кнопок множественного редактирования 
export function editMany() {
  document.querySelectorAll('#editMany').forEach(button => {
    button.addEventListener('click', event => {
      const form = event.target.parentElement
      const mainCheck = document.querySelector('.table th .form-check-input')

      if (!!mainCheck) {
        form.querySelector('[name="IDs"]').value = mainCheck.value
        form.submit()
      } else {
        Swal.fire({ 
          icon: 'warning', 
          title: 'Не удалось обнаружить выделенных элементов',
          customClass: { confirmButton: 'btn btn-primary' }
        })
      }
    })
  })
} 
