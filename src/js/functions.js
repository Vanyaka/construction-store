'use strict'

/**
 * Получение названия иконки
 * @param {Number} code HTTP-код ответа сервера
 * @returns название соответствующей иконки
 */
export function getIcon(code) {
  if (code >= 500) return 'error'
  if (code >= 400) return 'warning'
  if (code >= 300) return 'info'
  if (code >= 200) return 'success'
  return 'info'
}

/**
 * Получение предка
 * @param {*} element - DOM-елемент от которого ведётся отсчёт
 * @param {*} selectors - селекторы, по которым производится поиск
 * @returns предок (DOM-элемент), удовлетворяющий заданным селекторам
 */
export function getAncestor(element, selectors) {
  const parent = element.parentElement

  if (!parent) return undefined

  for (const key in selectors) {
    if (key == 'class' && !parent.classList.contains(selectors[key])) return getAncestor(parent, selectors)
    if (key == 'id' && parent.id != selectors[key]) return getAncestor(parent, selectors)
    if (key == 'name' && parent.name != selectors[key]) return getAncestor(parent, selectors)
  }

  return parent
}
