'use strict'

// Инициализация сортировки таблицы
export default function sortInit() {
  const buttons = document.querySelectorAll('.table thead .sortable')

  buttons.forEach(item => {
    item.addEventListener('click', event => {
      if (event.target.dataset['order'] == '0') {
        event.target.setAttribute('data-order', '1')
      } else if (event.target.dataset['order'] == '1') {
        event.target.setAttribute('data-order', '-1')
      } else if (event.target.dataset['order'] == '-1') {
        event.target.setAttribute('data-order', '0')
      } 
    })
  })
}
