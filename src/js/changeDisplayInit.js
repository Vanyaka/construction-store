'use strict'

import * as cookie from './cookie'
import { getAncestor } from './functions'

// Изменение кол-ва отображаемых на странице элементов
export default () => {
  const buttons = document.querySelectorAll('.display button')
  const display = cookie.get('display')

  buttons.forEach(item => {
    if (display == item.value) item.classList.add('active')

    item.addEventListener('click', event => {
      const target = (event.target.classList.contains('btn')) ? event.target : getAncestor(event.target, { class: 'btn' })

      deactivation(buttons)
      target.classList.add('active')
      cookie.set('display', target.value, { path: '/' })
      location.reload()
    })
  })

  if (!!buttons.length && !document.querySelectorAll('.display button.active').length) buttons[0].classList.add('active')
}

/**
 * Снятие "активности" с кнопок
 * @description удаляет у всех элементов класс "active"
 * @param {(Object|String)} data - NodeList c элементами | строка с селекторами
 */
function deactivation(data) {
  const elements = (data.toString() == '[object NodeList]') ? data : document.querySelectorAll(data)
  elements.forEach(item => item.classList.remove('active'))
}
