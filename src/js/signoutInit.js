'use strict'

import Swal from 'sweetalert2'
import { getIcon } from './functions'

// Инициализация кнопки выхода
export default function signoutInit() {
  const exitBtn = document.querySelector('.navbar #exit')

  if (!!exitBtn) exitBtn.addEventListener('click', async () => {
    try {
      const response = await fetch('/auth/signout', { 
        method: 'POST',
        headers: {
          "Accept": "application/json"
        }
      })
  
      try {
        const result = await response.json()
  
        if (response.ok) {
          location.reload()
        } else {
          Swal.fire({ 
            icon: getIcon(result.status), 
            title: result.msg,
            customClass: { confirmButton: 'btn btn-primary' }
          })
        }
      } catch (err) {
        Swal.fire({ 
          icon: 'error', 
          title: 'Не удалось расшифровать ответ сервера',
          customClass: { confirmButton: 'btn btn-primary' }
        })
      }
    } catch (err) {
    Swal.fire({ 
      icon: 'error', 
      title: 'Не удалось получить ответ от сервера',
      customClass: { confirmButton: 'btn btn-primary' }
    })
    }
  })
}
