'use strict'

// Инициализация блока загрузки изображений (с предпросмотром)
export default () => {
  document.querySelectorAll('.image-input-container').forEach(container => {
    const typeField = container.querySelector('.image-input-type')
    const fileFieldBlock = container.querySelector('.form-file.form-image')
    const urlField = container.querySelector('.form-control.form-image')
    const previewField = container.querySelector('.image-preview')

    // Обработчик смены типа поля
    typeField.addEventListener('change', (event) => {
      switch (event.target.value) {
        case 'file': 
          urlField.classList.remove('show')
          fileFieldBlock.classList.add('show')
          break
        case 'url': 
          fileFieldBlock.classList.remove('show')
          urlField.classList.add('show')
          break
      }
    })

    typeField.dispatchEvent(new Event('change'))
  
    // Обработчик обновления данных в полях
    container.querySelectorAll('.form-image').forEach(item => {
      item.addEventListener('change', event => {
        const field = event.target
        const fileField = fileFieldBlock.querySelector('input')

        if (!fileField.multiple) {
          if (field.classList.contains('form-file-input')) {
            urlField.value = ''
          } else if (field.classList.contains('form-control')) {
            fileField.value = ''
          }
        }

        previewField.innerHTML = ''

        if (!!fileField.files.length || !!urlField.value.length) {
          previewField.classList.add('show')
        } else {
          previewField.classList.remove('show')
        }

        for (const image of fileField.files) {
          const reader = new FileReader()
          const img = document.createElement('img')

          reader.onload = event => {
            img.setAttribute('src', event.target.result)
            img.setAttribute('title', image.name)

            previewField.appendChild(img)
          }

          reader.readAsDataURL(image)
        }

        for (const image of urlField.value.split(',')) {
          const img = document.createElement('img')
          img.setAttribute('src', image.trim())
          img.setAttribute('title', image.trim())

          previewField.appendChild(img)
        }
      })

      item.dispatchEvent(new Event('change'))
    }) 
  })
}
