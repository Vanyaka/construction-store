'use strict'

// Пакеты
import Swal from 'sweetalert2'
import { getIcon, getAncestor } from './functions'

// Инициализация отмечаемой таблицы
export function tableCheckable() {
  const mainCheck = document.querySelector('.table th .form-check-input')
  const checks = document.querySelectorAll('.table td .form-check-input')
  const oneToolbar = document.querySelector('#one-toolbar')
  const manyToolbar = document.querySelector('#many-toolbar')

  if (!!mainCheck && !!checks.length && !!oneToolbar && !!manyToolbar) {
    mainCheck.addEventListener('change', (event) => {
      checks.forEach(item => item.checked = event.target.checked)
      mainCheck.value = getValues('.table td .form-check-input:checked')
      toolBarChange([ oneToolbar, manyToolbar ], (!mainCheck.value) ? 0 : 1)
    })

    checks.forEach(item => {
      item.addEventListener('change', (event) => {
        mainCheck.checked = (document.querySelectorAll('.table td .form-check-input:checked').length < checks.length) ? false : true
        mainCheck.value = getValues('.table td .form-check-input:checked')
        toolBarChange([ oneToolbar, manyToolbar ], (!mainCheck.value) ? 0 : 1)
      })
    })
  }
}

// Инициализация отмечаемых полей формы
export function formCheckable() {
  const form = document.querySelector('form.checkable')

  if (!!form) {
    const checks = form.querySelectorAll('.input-group-text .form-check-input')
    const sendBtn = form.querySelector('#sendBtn')
  
    checks.forEach(item => {
      item.addEventListener('change', (event) => {
        const target = event.target
        const field = getAncestor(target, { class: 'input-group' }).querySelector('.input-group-field')
  
        if (target.checked) {
          field.disabled = false
          field.required = true
        } else {
          field.disabled = true
          field.required = false
        }
      })
    })

    sendBtn.addEventListener('click', async () => {
      const fields = form.querySelectorAll('.input-group-field')

      let bodyData = {}

      for (const field of fields) {
        if (!field.disabled) {
          bodyData[field.name] = field.value
        }
      }

      form.classList.add('was-validated')

      if(!Object.values(bodyData).filter(item => item === '').length) {
        const response = await fetch(location.href, {
          method: 'PATCH',
          headers: { 
            'Accept': 'application/json',
            'Content-Type': 'application/json'
           },
          body: JSON.stringify(bodyData)
        })
  
        try {
          const result = await response.json()
  
          if (response.ok) {
            location.href = result.url
          } else {
            Swal.fire({ 
              icon: getIcon(result.status), 
              title: result.msg,
              customClass: { confirmButton: 'btn btn-primary' }
            })
          }
        } catch (err) {
          Swal.fire({ 
            icon: 'error', 
            title: 'Не удалось расшифровать ответ сервера',
            customClass: { confirmButton: 'btn btn-primary' }
          })
        }
      }
    })
  }
}

// Возвращает массив значений элементов, найденных по заданнмоу селектору
function getValues(selector) {
  let result = []

  for (const item of document.querySelectorAll(selector)) {
    result.push(item.value)
  }

  return result
}

// Смена тулбара
function toolBarChange(toolbars, id) {
  for (let i = 0; i < toolbars.length; i++) {
    if (i == id) {
      toolbars[i].classList.add('show')
    } else {
      toolbars[i].classList.remove('show')
    }
  }
}
