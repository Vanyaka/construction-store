'use strict'

import * as cookie from './cookie'

// Изменение кол-ва отображаемых на странице элементов
export default function changeOnPageInit() {
  const buttons = document.querySelectorAll('.onPage button')
  const onPage = cookie.get('onPage')

  buttons.forEach(item => {
    if (onPage == item.value) item.classList.add('active')

    item.addEventListener('click', event => {
      deactivation(buttons)
      event.target.classList.add('active')
      cookie.set('onPage', event.target.value, { path: '/' })
      location.reload()
    })
  })

  if (!!buttons.length && !document.querySelectorAll('.onPage button.active').length) buttons[0].classList.add('active')
}

/**
 * Снятие "активности" с кнопок
 * @description удаляет у всех элементов класс "active"
 * @param {(Object|String)} data - NodeList c элементами | строка с селекторами
 */
function deactivation(data) {
  const elements = (data.toString() == '[object NodeList]') ? data : document.querySelectorAll(data)
  elements.forEach(item => item.classList.remove('active'))
}
