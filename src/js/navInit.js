'use strict'

// Инициализация навигационных панелей
export default function navInit() {
  for (const item of document.querySelectorAll('.sidebar .nav-link')) {
    if (item.href.split('#')[0].split('?')[0] === location.href.split('#')[0].split('?')[0]) {
      item.classList.add('active')
    } else {
      item.classList.remove('active')
    }
  }
}
