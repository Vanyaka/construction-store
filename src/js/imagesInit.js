'use strict'

// Пакеты
import Swal from 'sweetalert2'
import { getIcon, getAncestor } from './functions'

// Инициализация списка картинок
export default async () => {
  // Обработчики кнопок копирования
  document.querySelectorAll('.image-row .buttons .image-copy').forEach(item => {
    item.addEventListener('click', (event) => {
      if (!!navigator.clipboard) {
        navigator.clipboard.writeText(getAncestor(event.target, { class: 'image-row' }).querySelector('.image-link a').href)
      } else {
        Swal.fire({ 
          icon: 'warning', 
          title: 'Копирование в буфер доступно только при использовании HTTPS соединения',
          customClass: { confirmButton: 'btn btn-primary' }
        })
      }
    })
  })

  // Обработчики кнопок удаления
  document.querySelectorAll('.image-row .buttons .image-delete').forEach(item => {
    item.addEventListener('click', async (event) => {
      const response = await fetch(location.href, {
        method: 'DELETE',
        headers: { 
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ name: getAncestor(event.target, { class: 'image-row' }).querySelector('.image-name').innerText })
      })

      try {
        if (response.ok) {
          location.reload()
        } else {
          const result = await response.json()

          Swal.fire({ 
            icon: getIcon(result.status), 
            title: result.msg,
            customClass: { confirmButton: 'btn btn-primary' }
          })
        }
      } catch (err) {
        Swal.fire({ 
          icon: 'error', 
          title: 'Не удалось расшифровать ответ сервера',
          customClass: { confirmButton: 'btn btn-primary' }
        })
      }
    })
  })
}
