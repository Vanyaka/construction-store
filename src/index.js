import './scss/index.scss'
import * as feather from 'feather-icons'
import navInit from './js/navInit'
import changeOnPageInit from './js/changeOnPageInit'
import changeDisplayInit from './js/changeDisplayInit'
import sortInit from './js/sortInit'
import importInit from './js/importInit'
import tooltipInit from './js/tooltopsInit'
import validationInit from './js/validationInit'
import { deleteOne, deleteMany } from './js/deleteInit'
import { editMany } from './js/editInit'
import filterInit from './js/filterInit'
import { tableCheckable, formCheckable } from './js/checkableInit'
import imagesInit from './js/imagesInit'
import imageInputInit from './js/imageInputInit'
import selectManyInit from './js/selectManyInit'
import formFilePlaceholderInit from './js/formFilePlaceholderInit'

feather.replace()
navInit()
changeOnPageInit()
changeDisplayInit()
sortInit()
importInit()
tooltipInit()
validationInit()
tableCheckable()
formCheckable()
deleteOne()
deleteMany()
editMany()
filterInit()
imagesInit()
imageInputInit()
selectManyInit()
formFilePlaceholderInit()
