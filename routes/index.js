'use strict'

// Пакеты
const path = require('path')

// Модули
const catalog = require(path.join(__dirname, 'catalog'))
const auth = require(path.join(__dirname, 'auth'))
const adminPanel = require(path.join(__dirname, 'adminPanel'))
const categories = require(path.join(__dirname, 'categories'))
const api = require(path.join(__dirname, 'api'))
const other = require(path.join(__dirname, 'other'))

// Роутинг
module.exports = () => {
  app.use('/auth', auth) // Аутентификация

  app.use('/admin-panel', adminPanel) // Админ-панель

  app.use('/categories', categories) // Категории

  app.use('/api', api) // Технические запросы

  app.use('/', catalog) // Каталог

  app.use(other.NotFound) // Прочие
}
