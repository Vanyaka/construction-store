'use strict'

// Пакеты
const path = require('path')
const __rootpath = require('app-root-path').path
const express = require('express')

// Модули
const other = require(path.join(__rootpath, 'routes', 'other'))

let router = express.Router()

router.route('/:query') // Технические запросы
  .get(getMethod)
  .all(other.MethodNotAllowed)

/**
 * Вызов запрашиваемого метода
 */
async function getMethod(req, res, next) {
  try {
    switch (req.params.query) {
      case 'getMeasureNames': { // Получение списка уникальных ед. измерения
        res.json(await app.get('models').Article.distinct('measureName'))
        break
      }
      case 'getProducts': { // Получение всех товаров
        res.json(await app.get('models').Product.find())
        break
      }
      case 'getCategories': { // Получение всех категорий
        res.json(await app.get('models').Category.find())
        break
      }
      default: next(new HttpError(404))
    }
  } catch (err) {
    next(err)
  }
}

module.exports = router