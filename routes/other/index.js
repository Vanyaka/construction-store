'use strict'

// Не найдено
exports.NotFound = (req, res, next) => {
  next(new HttpError(404))
}

// Метод не поддерживается
exports.MethodNotAllowed = (req, res, next) => {
  next(new HttpError(405))
}
