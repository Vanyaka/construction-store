'use strict'

// Проверка ID
exports.checkID = (req, res, next) => {
  try {
    if (!app.get('regexes').ObjectID.test(req.params.id)) return next(new HttpError(404))
    next()
  } catch (err) {
    next(err)
  }
}

// Проверка на админа
exports.isAdmin = (req, res, next) => {
  try {
    if (!req.isAuthenticated() || req.user.role !== 'Администратор') return next(new HttpError(403))
    next()
  } catch (err) {
    next(err)
  }
}

// Проверка на хозяина
exports.isOwner = (req, res, next) => {
  try {
    if (!req.isAuthenticated() || req.user.id !== req.params.id) return next(new HttpError(403))
    next()
  } catch (err) {
    next(err)
  }
}

// Проверка на авторизованность
exports.isAuth = (req, res, next) => {
  try {
    if (!req.isAuthenticated()) return res.redirect('/auth/signin')
    next()
  } catch (err) {
    next(err)
  }
}
