'uae strict'

// Пакеты
const path = require('path')
const __rootpath = require('app-root-path').path
const express = require('express')

// Модули
const other = require(path.join(__rootpath, 'routes', 'other'))
const middlewares = require(path.join(__rootpath, 'routes', 'middlewares'))

let router = express.Router()

router.route('/') // Категории
  .get(getMainPage)
  .all(other.MethodNotAllowed)

router.route('/add') //  Добавление категории
  .get(getAddPage)
  .post(add)
  .all(other.MethodNotAllowed)

router.route('/:id') // Категория
  .all(middlewares.checkID)
  .get(getViewPage)
  .delete(remove)
  .all(other.MethodNotAllowed)

router.route('/:id/edit') // Редактирование категории
  .all(middlewares.checkID)
  .get(getEditPage)
  .post(edit)
  .all(other.MethodNotAllowed)

/**
 * Получение главной страницы
 */
async function getMainPage(req, res, next) {
  try {
    if (res.locals.display == 'table') {
      let sortParams = {}
      let query = {}

      // Парсинг фильтров и сортировок
      for (const param in req.urlData.params) {
        const value = req.urlData.params[param]

        if (param == 'name') {
          if (!query['$and']) query['$and'] = []
          let tmp = []
          for (const item of value) {
            tmp.push({ [param]: { $regex: `${(item.position == 'start') ? '^' : ''}${item.text.replace(app.get('regexes').specialSymbols, '\\$&')}${(item.position == 'end') ? '$' : ''}`, $options: 'i' } })
          }
          query['$and'].push({ $or: tmp })
        }

        if (param == 'parent') {
          if (!query['$and']) query['$and'] = []
          let tmp = []
          for (const item of value) {
            tmp.push({ [param]: item })
          }
          query['$and'].push({ $or: tmp })
        }
      }
  
      const data = await Promise.all([
        app.get('models').Category.find(query).sort(sortParams).skip(req.prePaginationData.onPage * (req.prePaginationData.page - 1)).limit(req.prePaginationData.onPage).populate('parent'),
        pagination(app.get('models').Category, query, req.urlData, req.prePaginationData.onPage, req.prePaginationData.page)
      ])
  
      res.render('adminPanel/categories', {
        categories: data[0],
        pagination_data: data[1]
      })
    } else {
      res.render('adminPanel/categories', {
        tree: routHelpers.getChildrenTree(await app.get('models').Category.find(), undefined)
      })
    }
  } catch (err) {
    next(err)
  }
}

/**
 * Получение страницы добавления
 */
async function getAddPage(req, res, next) {
  try {
    res.render('adminPanel/categories/add', {
      categories: routHelpers.getChildrenTree(await app.get('models').Category.find(), undefined)
    })
  } catch (err) {
    next(err)
  }
}

/**
 * Получение страницы просмотра
 */
async function getViewPage(req, res, next) {
  try {
    const category = await app.get('models').Category.findById(req.params.id).populate('parent')

    if (!category) return next(new HttpError(404))

    res.render('adminPanel/categories/view', {
      category: category
    })
  } catch (err) {
    next(err)
  }
}

/**
 * Получение страницы редактирования
 */
async function getEditPage(req, res, next) {
  try {
    const data = await Promise.all([
      app.get('models').Category.findById(req.params.id).populate('parent'),
      app.get('models').Category.find()
    ])

    if (/^(\/img\/upload\/)/.test(data[0].image)) data[0].image = `${res.locals.origin}${data[0].image}`
    
    res.render('adminPanel/categories/edit', {
      category: data[0],
      categories: routHelpers.getChildrenTree(data[1], undefined)
    })
  } catch (err) {
    next(err)
  }
}

/**
 * Добавление
 */
async function add(req, res, next) {
  try {
    for (const key in req.body) {
      if (typeof req.body[key] === 'string') req.body[key] = req.body[key].trim()
      if (req.body[key] === '') req.body[key] = undefined
    }

    if (!req.body.name) return next(new HttpError(400, 'Заполните все обязательные поля'))
    if (!!req.body.parent && !app.get('regexes').ObjectID.test(req.body.parent)) return next(new HttpError(400, 'Переданы некорректный ID родителя'))
    if ((!!req.files && !!req.files.image && ![ '.jpeg', '.jpg', '.png', '.svg', '.gif' ].includes(path.extname(req.files.image.name))) || (!!req.body.image && (!app.get('regexes').URL.test(req.body.image.split(',')[0].trim()) || ![ '.jpeg', '.jpg', '.png', '.svg', '.gif' ].includes(path.extname(req.body.image.split(',')[0].trim()))))) return next(new HttpError(400, 'Недопустимый формат. Используйте картинки с допустимым расширением: .jpeg, .jpg, .png, .svg, .gif'))
    if (!!req.files && !!req.files.image && req.files.image.size > (3 * 1024 * 1024)) return next(new HttpError(400, 'Слишком большой размер картинки. Для загрузки используйте файлы размером до 3 мб'))

    let promises = [ app.get('models').Category.findOne({ name: req.body.name }) ]
    if (!!req.body.parent) promises.push(app.get('models').Category.findById(req.body.parent))
    if (!!req.files && !!req.files.image) promises.push(req.files.image.mv(path.join(__rootpath, 'dist', 'img', 'upload', req.files.image.name))) 

    const [ rival, parent ] = await Promise.all(promises)

    if (!!req.body.parent && !parent) return next(new HttpError(400, 'Указанной родительской категории не сущестует'))
    if (!!rival) return next(new HttpError(400, 'Категория с таким названием уже существует'))

    const category = new (app.get('models').Category)({
      name: req.body.name,
      parent: (!!req.body.parent) ? req.body.parent : undefined,
      image: (!!req.files && !!req.files.image) ? `/img/upload/${req.files.image.name}` : (!!req.body.image) ? req.body.image.split(',')[0].trim().replace(res.locals.origin, '') : undefined,
      description: (!!req.body.description) ? req.body.description : undefined
    })

    await category.save()

    res.redirect('/admin-panel/categories')
  } catch (err) {
    next(err)
  }
}

/**
 * Редактирование
 */
async function edit(req, res, next) {
  try {
    for (const key in req.body) {
      if (typeof req.body[key] === 'string') req.body[key] = req.body[key].trim()
      if (req.body[key] === '') req.body[key] = undefined
    }

    if (!!req.body.parent && !!req.body.parent && !app.get('regexes').ObjectID.test(req.body.parent)) return next(new HttpError(400, 'Переданы некорректные данные'))
    if ((!!req.files && !!req.files.image && ![ '.jpeg', '.jpg', '.png', '.svg', '.gif' ].includes(path.extname(req.files.image.name))) || (!!req.body.image && (!app.get('regexes').URL.test(req.body.image.split(',')[0].trim()) || ![ '.jpeg', '.jpg', '.png', '.svg', '.gif' ].includes(path.extname(req.body.image.split(',')[0].trim()))))) return next(new HttpError(400, 'Недопустимый формат. Используйте картинки с допустимым расширением: .jpeg, .jpg, .png, .svg, .gif'))
    if (!!req.files && !!req.files.image && req.files.image.size > (3 * 1024 * 1024)) return next(new HttpError(400, 'Слишком большой размер картинки. Для загрузки используйте файлы размером до 3 мб'))

    let promises = [ app.get('models').Category.findById(req.params.id) ]
    if (!!req.body.parent) promises.push(app.get('models').Category.findById(req.body.parent))
    if (!!req.files && !!req.files.image) promises.push(req.files.image.mv(path.join(__rootpath, 'dist', 'img', 'upload', req.files.image.name))) 

    const [ category, parent ] = await Promise.all(promises)

    if (!category) return next(new HttpError(404))

    let update = { $unset: {} }
    const schema = app.get('models').Category.schema.obj

    if (!!req.body.parent && !parent) return next(new HttpError(400, 'Указанной родительской категории не сущестует'))

    if (!!req.files && !!req.files.image) {
      if (category['image'] != `/img/upload/${req.files.image.name}`) update['image'] = `/img/upload/${req.files.image.name}`
    } else if (!!req.body.image) {
      if (category['image'] != req.body.image.split(',')[0].trim().replace(res.locals.origin, '')) update['image'] = req.body.image.split(',')[0].trim().replace(res.locals.origin, '')
    } else if (!!category['image']) {
      update['$unset']['image'] = 1
    }

    delete req.body['image']

    for (const item in req.body) {
      if (!req.body[item]) {
        if (!!schema[item].required) {
          return next(new HttpError(400, 'Переданы некорректные данные'))
        } else if (!!category[item]) {
          update['$unset'][item] = 1
        }
      } else {
        if (item in schema && category[item] != req.body[item]) update[item] = req.body[item]
      }
    }

    if (!!Object.keys(update).length) await app.get('models').Category.updateOne({ _id: req.params.id }, update)
    
    res.redirect(`/admin-panel/categories/${req.params.id}`)
  } catch (err) {
    next(err)
  }
}

/**
 * Удаление
 */
async function remove(req, res, next) {
  try {
    await Promise.all([
      app.get('models').Product.updateMany({ category: req.params.id }, { $unset: { category: 1 } }),
      app.get('models').Category.updateMany({ parent: req.params.id }, { $unset: { parent: 1 } }),
      app.get('models').Category.deleteOne({ _id: req.params.id })
    ])

    res.json({ url: '/admin-panel/categories' })
  } catch (err) {
    next(err)
  }
}

module.exports = router
