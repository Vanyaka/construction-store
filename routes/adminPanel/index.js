'uae strict'

// Пакеты
const path = require('path')
const __rootpath = require('app-root-path').path
const express = require('express')

// Модули
const statistic = require(path.join(__dirname, 'statistic'))
const orders = require(path.join(__dirname, 'orders'))
const users = require(path.join(__dirname, 'users'))
const importData = require(path.join(__dirname, 'import'))
const settings = require(path.join(__dirname, 'settings'))
const categories = require(path.join(__dirname, 'categories'))
const products = require(path.join(__dirname, 'products'))
const articles = require(path.join(__dirname, 'articles'))
const images = require(path.join(__dirname, 'images'))
const other = require(path.join(__rootpath, 'routes', 'other'))
const middlewares = require(path.join(__rootpath, 'routes', 'middlewares'))

let router = express.Router()

router.route('*') // Ограничение доступа к Админ-панели
  .all(middlewares.isAuth, middlewares.isAdmin)

router.route('/') // Админ-панель
  .get(getPage)
  .all(other.MethodNotAllowed)

router.use('/statistic', statistic) // Ститистика

router.use('/orders', orders) // Заказы

router.use('/users', users) // Пользователи

router.use('/import', importData) // Импорт

router.use('/settings', settings) // Настройки

router.use('/categories', categories) // Категории

router.use('/products', products) // Товары

router.use('/articles', articles) // Артикулы

router.use('/images', images) // Картинки

/**
 * Получение страницы
 */
async function getPage(req, res, next) {
  try {
    res.render('adminPanel')
  } catch (err) {
    next(err)
  }
}

module.exports = router