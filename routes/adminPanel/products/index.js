'uae strict'

// Пакеты
const path = require('path')
const __rootpath = require('app-root-path').path
const express = require('express')
const mongoose = require('mongoose')

// Модули
const other = require(path.join(__rootpath, 'routes', 'other'))
const middlewares = require(path.join(__rootpath, 'routes', 'middlewares'))

let router = express.Router()

router.route('/') // Товары
  .get(getMainPage)
  .all(other.MethodNotAllowed)

router.route('/add') // Добавление товара
  .get(getAddPage)
  .post(add)
  .all(other.MethodNotAllowed)

router.route('/:id') // Товар
  .all(middlewares.checkID)
  .get(getViewPage)
  .delete(remove)
  .all(other.MethodNotAllowed)

router.route('/:id/edit') // Редактирование товара
  .all(middlewares.checkID)
  .get(getEditPage)
  .post(edit)
  .all(other.MethodNotAllowed)

/**
 * Получение главной страницы
 */
async function getMainPage(req, res, next) {
  try {
    let sortParams = {}
    let query = {}

    // Парсинг фильтров и сортировок
    for (const param in req.urlData.params) {
      const value = req.urlData.params[param]

      if (param == 'name') {
        if (!query['$and']) query['$and'] = []
        let tmp = []
        for (const item of value) {
          tmp.push({ [param]: { $regex: `${(item.position == 'start') ? '^' : ''}${item.text.replace(app.get('regexes').specialSymbols, '\\$&')}${(item.position == 'end') ? '$' : ''}`, $options: 'i' } })
        }
        query['$and'].push({ $or: tmp })
      }

      if (param == 'category') {
        if (!query['$and']) query['$and'] = []
        let tmp = []
        for (const item of value) {
          tmp.push({ categories: { $all: [item] } })
        }
        query['$and'].push({ $or: tmp })
      }
    }

    const data = await Promise.all([
      app.get('models').Product.find(query).sort(sortParams).skip(req.prePaginationData.onPage * (req.prePaginationData.page - 1)).limit(req.prePaginationData.onPage).populate('categories'),
      pagination(app.get('models').Product, query, req.urlData, req.prePaginationData.onPage, req.prePaginationData.page)
    ])

    res.render('adminPanel/products', {
      products: data[0],
      pagination_data: data[1]
    })
  } catch (err) {
    next(err)
  }
}

/**
 * Получение страницы добавления
 */
async function getAddPage(req, res, next) {
  try {
    res.render('adminPanel/products/add', {
      categories: await app.get('models').Category.find()
    })
  } catch (err) {
    next(err)
  }
}

/**
 * Получение страницы просмотра
 */
async function getViewPage(req, res, next) {
  try {
    const data = await Promise.all([
      app.get('models').Product.findById(req.params.id).populate('categories'),
      app.get('models').Article.countDocuments({ product: req.params.id })
    ])

    if (!data[0]) return next(new HttpError(404))

    res.render('adminPanel/products/view', {
      product: data[0],
      articlesCount: data[1],
      images: (!!data[0].images) ? data[0].images.split(',').map(item => item.trim()) : []
    })
  } catch (err) {
    next(err)
  }
}

/**
 * Получение страницы редактирования
 */
async function getEditPage(req, res, next) {
  try {
    const data = await Promise.all([
      app.get('models').Product.findById(req.params.id),
      app.get('models').Category.find()
    ])

    if (!!data[0].images) data[0].images = data[0].images.split(',').map(item => (/^(\/img\/upload\/)/.test(item)) ? `${res.locals.origin}${item}` : item).toString()

    res.render('adminPanel/products/edit', {
      product: data[0],
      categories: data[1]
    })
  } catch (err) {
    next(err)
  }
}

/**
 * Добавление
 */
async function add(req, res, next) {
  try {
    for (const key in req.body) {
      if (typeof req.body[key] === 'string') req.body[key] = req.body[key].trim()
      if (req.body[key] === '') req.body[key] = undefined
    }

    if (!!req.files && !!req.files.images && !Array.isArray(req.files.images)) req.files.images = [ req.files.images ]

    if (!req.body.name) return next(new HttpError(400, 'Заполните все обязательные поля'))
    if (!!req.body.categories) {
      for (const category of req.body.categories.split(',')) {
        if (!app.get('regexes').ObjectID.test(category)) return next(new HttpError(400, 'Переданы некорректные данные'))
      }
    }
    if (!!req.body.images) {
      for (const image of req.body.images.split(',')) {
        if (!app.get('regexes').URL.test(image.trim()) || ![ '.jpeg', '.jpg', '.png', '.svg', '.gif' ].includes(path.extname(image.trim()))) return next(new HttpError(400, 'Недопустимый формат. Используйте картинки с допустимым расширением: .jpeg, .jpg, .png, .svg, .gif'))
      }
    }
    if (!!req.files && !!req.files.images) {
      for (const image of req.files.images) {
        if (![ '.jpeg', '.jpg', '.png', '.svg', '.gif' ].includes(path.extname(image.name))) return next(new HttpError(400, 'Недопустимый формат. Используйте картинки с допустимым расширением: .jpeg, .jpg, .png, .svg, .gif'))
        if (image.size > (3 * 1024 * 1024)) return next(new HttpError(400, 'Слишком большой размер картинки. Для загрузки используйте файлы размером до 3 мб'))
      }
    }

    let promises = [ app.get('models').Product.findOne({ name: req.body.name }) ]
    if (!!req.body.categories) promises.push(app.get('models').Category.find({ _id: { $in: req.body.categories.split(',') } }))
    if (!!req.files && !!req.files.images) {
      for (const image of req.files.images) {
        promises.push(image.mv(path.join(__rootpath, 'dist', 'img', 'upload', image.name))) 
      }
    }

    const [ rival, categories ] = await Promise.all(promises)

    if (!!rival) return next(new HttpError(400, 'Товар с таким названием уже существует'))
    if (!!req.body.categories && req.body.categories.split(',').length !==  categories.length) return next(new HttpError(400, 'Указанной категории не сущестует'))

    let images = ''
    if (!!req.files) for (const image of req.files.images) {
      images += `/img/upload/${image.name},`
    }

    if (!!req.body.images) for (const image of req.body.images.split(',')) {
      images += `${image.trim().replace(res.locals.origin, '')},`
    }

    const product = new (app.get('models').Product)({
      name: req.body.name,
      categories: (!!req.body.categories) ? req.body.categories.split(',') : undefined,
      images: (!!images.length) ? images.replace(/\,$/, '') : undefined,
      description: (!!req.body.description) ? req.body.description : undefined,
      fullDescription: (!!req.body.fullDescription) ? req.body.fullDescription : undefined
    })

    await product.save()

    res.redirect('/admin-panel/products')
  } catch (err) {
    next(err)
  }
}

/**
 * Редактирование
 */
async function edit(req, res, next) {
  try {
    for (const key in req.body) {
      if (typeof req.body[key] === 'string') req.body[key] = req.body[key].trim()
      if (req.body[key] === '') req.body[key] = undefined
    }

    if (!!req.files && !!req.files.images && !Array.isArray(req.files.images)) req.files.images = [ req.files.images ]

    if (!req.body.name) return next(new HttpError(400, 'Заполните все обязательные поля'))
    if (!!req.body.categories) {
      for (const category of req.body.categories.split(',')) {
        if (!app.get('regexes').ObjectID.test(category)) return next(new HttpError(400, 'Переданы некорректные данные'))
      }
    }
    if (!!req.body.images) {
      for (const image of req.body.images.split(',')) {
        if (!app.get('regexes').URL.test(image.trim()) || ![ '.jpeg', '.jpg', '.png', '.svg', '.gif' ].includes(path.extname(image.trim()))) return next(new HttpError(400, 'Недопустимый формат. Используйте картинки с допустимым расширением: .jpeg, .jpg, .png, .svg, .gif'))
      }
    }
    if (!!req.files && !!req.files.images) {
      for (const image of req.files.images) {
        if (![ '.jpeg', '.jpg', '.png', '.svg', '.gif' ].includes(path.extname(image.name))) return next(new HttpError(400, 'Недопустимый формат. Используйте картинки с допустимым расширением: .jpeg, .jpg, .png, .svg, .gif'))
        if (image.size > (3 * 1024 * 1024)) return next(new HttpError(400, 'Слишком большой размер картинки. Для загрузки используйте файлы размером до 3 мб'))
      }
    }

    let promises = [ app.get('models').Product.findById(req.params.id), app.get('models').Product.findOne({ name: req.body.name }) ]
    if (!!req.body.categories) promises.push(app.get('models').Category.find({ _id: { $in: req.body.categories.split(',') } }))
    if (!!req.files && !!req.files.images) {
      for (const image of req.files.images) {
        promises.push(image.mv(path.join(__rootpath, 'dist', 'img', 'upload', image.name))) 
      }
    }

    const [ product, rival, categories ] = await Promise.all(promises)

    if (!product) return next(new HttpError(404))

    let update = { $unset: {} }
    const schema = app.get('models').Product.schema.obj
    
    if (!!rival && rival.id != product.id) return next(new HttpError(400, 'Товар с таким названием уже существует'))
    if (!!req.body.categories && req.body.categories.split(',').length !==  categories.length) return next(new HttpError(400, 'Указанной категории не сущестует'))

    let images = ''
    if (!!req.files) for (const image of req.files.images) {
      images += `/img/upload/${image.name},`
    }

    if (!!req.body.images) for (const image of req.body.images.split(',')) {
      images += `${image.trim().replace(res.locals.origin, '')},`
    }

    if (!!req.body.images) req.body.images = images.replace(/\,$/, '')

    for (const item in req.body) {
      if (!req.body[item]) {
        if (!!schema[item].required) {
          return next(new HttpError(400, 'Переданы некорректные данные'))
        } else if (!!product[item]) {
          update['$unset'][item] = 1
        }
      } else if (item in schema && product[item] != req.body[item]) update[item] = (item == 'categories') ? req.body[item].split(',') : req.body[item]
    }

    if (!!Object.keys(update).length) await app.get('models').Product.updateOne({ _id: req.params.id }, update)
    
    res.redirect(`/admin-panel/products/${req.params.id}`)
  } catch (err) {
    next(err)
  }
}

/**
 * Удаление
 */
async function remove(req, res, next) {
  try {
    await Promise.all([
      app.get('models').Article.updateMany({ product: req.params.id }, { $unset: { product: 1 } }),
      app.get('models').Product.deleteOne({ _id: req.params.id })
    ])

    res.json({ url: '/admin-panel/products' })
  } catch (err) {
    next(err)
  }
}

module.exports = router
