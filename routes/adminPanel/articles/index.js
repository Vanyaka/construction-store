'uae strict'

// Пакеты
const qs = require('qs')
const path = require('path')
const __rootpath = require('app-root-path').path
const express = require('express')

// Модули
const other = require(path.join(__rootpath, 'routes', 'other'))
const middlewares = require(path.join(__rootpath, 'routes', 'middlewares'))

let router = express.Router()

router.route('/') // Список артикулов
  .get(getMainPage)
  .delete(removeMany)
  .all(other.MethodNotAllowed)

router.route('/add') // Добавление артикула
  .get(getAddPage)
  .post(add)
  .all(other.MethodNotAllowed)

router.route('/edit') // Редактирование артикулов
  .post(getEditManyPage)
  .patch(editMany)
  .all(other.MethodNotAllowed)

router.route('/:id') // Артикул
  .all(middlewares.checkID)
  .get(getViewPage)
  .delete(remove)
  .all(other.MethodNotAllowed)

router.route('/:id/edit') // Редактирование артикула
  .all(middlewares.checkID)
  .get(getEditPage)
  .post(edit)
  .all(other.MethodNotAllowed)

/**
 * Получение главной страницы
 */
async function getMainPage(req, res, next) {
  try {
    let sortParams = {}
    let query = {}

    // Парсинг фильтров и сортировок
    for (const param in req.urlData.params) {
      const value = req.urlData.params[param]

      if (param == 'name') {
        if (!query['$and']) query['$and'] = []
        let tmp = []
        for (const item of value) {
          tmp.push({ [param]: { $regex: `${(item.position == 'start') ? '^' : ''}${item.text.replace(app.get('regexes').specialSymbols, '\\$&')}${(item.position == 'end') ? '$' : ''}`, $options: 'i' } })
        }
        query['$and'].push({ $or: tmp })
      }

      if ([ 'quantity', 'price', 'costPrice', 'code'  ].includes(param)) {
        if (!query['$and']) query['$and'] = []
        let tmp = []
        for (const item of value) {
          let tmp2 = []
          if (item.min != undefined) tmp2.push({ [param]: { $gte: item.min } })
          if (item.max != undefined) tmp2.push({ [param]: { $lte: item.max } })
          tmp.push({ $and: tmp2 })
        }
        query['$and'].push({ $or: tmp })
      }

      if ([ 'product', 'measureName' ].includes(param)) {
        if (!query['$and']) query['$and'] = []
        let tmp = []
        for (const item of value) {
          tmp.push({ [param]: item })
        }
        query['$and'].push({ $or: tmp })
      }

      if (param == 'active') {
        if (!query['$and']) query['$and'] = []
        let tmp = []
        for (const item of value) {
          if (item == 'active') tmp.push({ active: true })
          if (item == 'disabled') tmp.push({ active: false })
        }
        query['$and'].push({ $or: tmp })
      }

    //   if (/^sort/.test(param) && ['-1', '1'].includes(value)) {
    //     if (param === 'sortByName') sortParams['name'] = value
    //     if (param === 'sortByQuantity') sortParams['quantity'] = value
    //     if (param === 'sortByPrice') sortParams['price'] = value
    //     if (param === 'sortByCostPrice') sortParams['costPrice'] = value
    //   }

    //   if (param === 'active' && ['true', 'false', '1', '0'].includes(value)) query['active'] = value
    //   if (param === 'measureName' && value !== '') query['measureName'] = value
    //   if (param === 'product' && ['true', 'false', '1', '0'].includes(value)) query['product'] = { $exists: value }
      
    //   if (/(GTE|LTE)$/.test(param) && value !== ''  && !isNaN(Number(value))) {
    //     if (param === 'quantityGTE') (!query['quantity']) ? query['quantity'] = { $gte: value } :  query['quantity']['$gte'] = value
    //     if (param === 'quantityLTE') (!query['quantity']) ? query['quantity'] = { $lte: value } :  query['quantity']['$lte'] = value
    //     if (param === 'priceGTE') (!query['price']) ? query['price'] = { $gte: value } :  query['price']['$gte'] = value
    //     if (param === 'priceLTE') (!query['price']) ? query['price'] = { $lte: value } :  query['price']['$lte'] = value
    //     if (param === 'costPriceGTE') (!query['costPrice']) ? query['costPrice'] = { $gte: value } :  query['costPrice']['$gte'] = value
    //     if (param === 'costPriceLTE') (!query['costPrice']) ? query['costPrice'] = { $lte: value } :  query['costPrice']['$lte'] = value
    //   }
    }

    const data = await Promise.all([
      app.get('models').Article.find(query).sort(sortParams).skip(req.prePaginationData.onPage * (req.prePaginationData.page - 1)).limit(req.prePaginationData.onPage).populate('product'),
      pagination(app.get('models').Article, query, req.urlData, req.prePaginationData.onPage, req.prePaginationData.page)
    ])

    res.render('adminPanel/articles', {
      articles: data[0],
      pagination_data: data[1]
    })
  } catch (err) {
    next(err)
  }
}

/**
 * Получение страницы добавления
 */
async function getAddPage(req, res, next) {
  try {
    res.render('adminPanel/articles/add', {
      products: await app.get('models').Product.find()
    })
  } catch (err) {
    next(err)
  }
}

/**
 * Получение страницы просмотра
 */
async function getViewPage(req, res, next) {
  try {
    const article = await app.get('models').Article.findById(req.params.id).populate('product')

    if (!article) return next(new HttpError(404))

    res.render('adminPanel/articles/view', {
      article: article
    })
  } catch (err) {
    next(err)
  }
}

/**
 * Получение страницы редактирования
 */
async function getEditPage(req, res, next) {
  try {
    const data = await Promise.all([
      app.get('models').Article.findById(req.params.id).populate('product'),
      app.get('models').Product.find()
    ])

    res.render('adminPanel/articles/edit', {
      article: data[0],
      products: data[1]
    })
  } catch (err) {
    next(err)
  }
}

/**
 * Получение страницы множественного редактирования
 */
async function getEditManyPage(req, res, next) {
  try {
    res.render('adminPanel/articles/editMany', { 
      IDs: req.body.IDs.split(','),
      products: await app.get('models').Product.find()
    })
  } catch (err) {
    next(err)
  }
}

/**
 * Добавление
 */
async function add(req, res, next) {
  try {
    for (const key in req.body) {
      if (typeof req.body[key] === 'string') req.body[key] = req.body[key].trim()
      if (req.body[key] === '') req.body[key] = undefined
    }

    if (!req.body.uuid || !req.body.name || !req.body.measureName || !req.body.quantity || !req.body.price || !req.body.costPrice || !req.body.code || !req.body.barCode) return next(new HttpError(400, 'Заполните все обязательные поля'))
    if (!!req.body.product && !app.get('regexes').ObjectID.test(req.body.product)) return next(new HttpError(400, 'Переданы некорректные данные'))

    let promises = [ app.get('models').Article.findOne({$or: [{uuid: req.body.uuid}, {code: req.body.code}, {barCode: req.body.barCode}]}) ]
    if (!!req.body.product) promises.push(app.get('models').Product.findById(req.body.product))

    const [ rival, product ] = await Promise.all(promises)

    if (!!req.body.product && !product) return next(new HttpError(400, 'Указанного товара не существует'))
    if (!!rival) return next(new HttpError(400, `Уже существует артикул с указанными полями: ${getConfrontationProperties(req.body, rival, app.get('models').Article.schema.obj).join(', ')}`))

    const article = new (app.get('models').Article)({
      uuid: req.body.uuid,
      name: req.body.name,
      product: req.body.product,
      measureName: req.body.measureName,
      quantity: req.body.quantity,
      price: req.body.price,
      costPrice: req.body.costPrice,
      code: req.body.code,
      barCode: req.body.barCode,
      active: !!req.body.active
    })

    await article.save()

    res.redirect('/admin-panel/articles')
  } catch (err) {
    next(err)
  }
}

/**
 * Реактирование
 */
async function edit(req, res, next) {
  try {
    for (const key in req.body) {
      if (typeof req.body[key] == 'string') req.body[key] = req.body[key].trim()
      if (req.body[key] === '') req.body[key] = undefined
    }

    let promises = [ app.get('models').Article.findById(req.params.id) ]
    if (!!req.body.product) promises.push(app.get('models').Product.findById(req.body.product))

    const [ article, product ] = await Promise.all(promises)

    if (!article) return next(new HttpError(404))

    let update = {}
    const schema = app.get('models').Article.schema.obj

    if (!!req.body.product && !app.get('regexes').ObjectID.test(req.body.product)) return next(new HttpError(400, 'Переданы некорректные данные'))
    if (!!req.body.product && !product) return next(new HttpError(400, 'Указанный товар не сущестует'))

    if (article.product != req.body.product) {
      if (req.body.product === undefined) {
        update['$unset'] = { product: 1 }
      } else {
        update.product = req.body.product
      }
    } 

    delete req.body.product
    req.body.active = !!req.body.active

    for (const item in req.body) {
      if (req.body[item] === undefined) return next(new HttpError(400, 'Переданы некорректные данные'))
      if (item in schema && article[item] != req.body[item]) update[item] = req.body[item]
    }

    if (!!Object.keys(update).length) await app.get('models').Article.updateOne({ _id: req.params.id }, update)
    
    res.redirect(`/admin-panel/articles/${req.params.id}`)
  } catch (err) {
    next(err)
  }
}

/**
 * Удаление
 */
async function remove(req, res, next) {
  try {
    await app.get('models').Article.deleteOne({ _id: req.params.id })

    res.json({ url: '/admin-panel/articles' })
  } catch (err) {
    next(err)
  }
}

/**
 * Множественное редактирование
 */
async function editMany(req, res, next) {
  try {
    for (const key in req.body) {
      if (typeof req.body[key] == 'string') req.body[key] = req.body[key].trim()
      if (req.body[key] === '') req.body[key] = undefined
    }

    let IDs = req.body.IDs.split(',')
    let update = {}
    const schema = app.get('models').Article.schema.obj

    for (const ID of IDs) {
      if (!app.get('regexes').ObjectID.test(ID)) return next(new HttpError(400, 'Передан некорректный ID'))
    }
    
    if (!!req.body.hasOwnProperty('product')) {
      if (!!req.body.product) {
        if (!app.get('regexes').ObjectID.test(req.body.product)) return next(new HttpError(400, 'Переданы некорректные данные'))
        if (! await app.get('models').Product.findById(req.body.product)) return next(new HttpError(400, 'Указанный товар не сущестует'))
        update.product = req.body.product
      } else {
        update['$unset'] = { product: 1 }
      }
    }

    if (!!req.body.hasOwnProperty('active') && [ 'true', 'false' ].includes(req.body.active)) update['active'] = (req.body.active == 'true')

    delete req.body.IDs
    delete req.body.product
    delete req.body.active

    for (const item in req.body) {
      if (req.body[item] === undefined) return next(new HttpError(400, 'Переданы некорректные данные'))
      if (item in schema) update[item] = req.body[item]
    }

    await app.get('models').Article.updateMany({ _id: { $in: IDs } }, update)

    res.json({ url: `/admin-panel/articles` })
  } catch (err) {
    next(err)
  }
}

/**
 * Множественное удаление
 */
async function removeMany(req, res, next) {
  try {
    if (!req.body.IDs || !req.body.IDs.length) return next(new HttpError(400, 'Не указан список ID для удаления'))

    let IDs = []

    for (const ID of req.body.IDs) {
      if (!app.get('regexes').ObjectID.test(ID)) return next(new HttpError(400, 'Передан некорректный ID'))
      IDs.push(ID)
    }

    await app.get('models').Article.deleteMany({ _id: { $in: IDs } })

    delete req.urlData.params['start']
    
    res.json({ url: `${req.urlData.pathname}?${qs.stringify(req.urlData.params)}` })
  } catch (err) {
    next(err)
  }
}

/**
 * Выявление конфликтующих полей в доукментах одной модели
 * @param {*} doc1 документ №1
 * @param {*} doc2 документ №2
 * @param {*} schema схема модели
 * @returns массив полей, имеющих свойство 'unique: true' и одинаковое значение в обоих документах
 */
function getConfrontationProperties(doc1, doc2, schema) {
  let result = []

  for (const key in schema) {
    if (!!schema[key].unique && doc1[key] == doc2[key]) result.push(key)
  }

  return result
}

module.exports = router