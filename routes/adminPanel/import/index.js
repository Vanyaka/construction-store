'uae strict'

// Пакеты
const path = require('path')
const __rootpath = require('app-root-path').path
const express = require('express')
const XLSX = require('xlsx')

// Модули
const other = require(path.join(__rootpath, 'routes', 'other'))

let router = express.Router()

router.route('/') // Импорт
  .get(getPage)
  .post(add)
  .patch(update)
  .all(other.MethodNotAllowed)

/**
 * Получение страницы
 */
function getPage(req, res, next) {
  try {
    res.render('adminPanel/import')
  } catch (err) {
    next(err)
  }
}

/**
 * Добавление данных из .xls(x) файла
 */
async function add(req, res, next) {
  try {
    if (!req.files || !req.files.data) return next(new HttpError(400, "Отсутствует файл для импорта"))
    if ([ '.xls', '.xlsx' ].indexOf(path.extname(req.files.data.name)) == -1) return next(new HttpError(400, 'Недопустимый формат. Используйте для импорта файлы с расширением .xls или .xlsx'))
    if (req.files.data.size > (10 * 1024 * 1024)) return next(new HttpError(400, 'Слишком большой файл. Для импорта используйте файлы размером до 10 мб'))

    const workbook = XLSX.read(req.files.data.data)
    const data = XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]])

    const uuids = await app.get('models').Article.distinct('uuid')
    let log = { add: [], fails: [] }

    for (const item of data) {
      if (!item.group) {
        for (const key in item) {
          if (typeof item[key] === 'string') item[key] = item[key].trim()
        }

        if (!uuids.includes(item.uuid) && !log['add'].includes(item.uuid)) {
          const article = new (app.get('models').Article)({
            uuid: item.uuid,
            name: item.name,
            measureName: item.measureName,
            quantity: item.quantity,
            price: item.price,
            costPrice: item.costPrice,
            code: item.code,
            barCode: item.barCodes,
            active: (item.quantity > 0) ? true : false
          })

          await article.save()

          log['add'].push(item.uuid)
        } else {
          log['fails'].push(item.uuid)
        }
      }
    }

    res.json({
      add: log['add'].length,
      fails: log['fails']
    })
  } catch (err) {
    next(err)
  }
} 

/**
 * Обновление данных из .xls(x) файла
 */
async function update(req, res, next) {
  try {
    if (!req.files || !req.files.data) return next(new HttpError(400, "Отсутствует файл для импорта"))
    if ([ '.xls', '.xlsx' ].indexOf(path.extname(req.files.data.name)) == -1) return next(new HttpError(400, 'Недопустимый формат. Используйте для импорта файлы с расширением .xls или .xlsx'))
    if (req.files.data.size > (10 * 1024 * 1024)) return next(new HttpError(400, 'Слишком большой файл. Для импорта используйте файлы размером до 10 мб'))

    const workbook = XLSX.read(req.files.data.data)
    const data = XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]])

    let currentArticles = []
    let log = { add: [], update: [], hide: [] }

    for (const item of data) {
      if (!item.group) {
        for (const key in item) {
          if (typeof item[key] === 'string') item[key] = item[key].trim()
        }
        
        currentArticles.push(item.uuid)

        let article = await app.get('models').Article.findOne({ uuid: item.uuid })
        let update = {}

        if (!article) {
          article = new (app.get('models').Article)({
            uuid: item.uuid,
            name: item.name,
            measureName: item.measureName,
            quantity: item.quantity,
            price: item.price,
            costPrice: item.costPrice,
            code: item.code,
            barCode: item.barCodes,
            active: (item.quantity > 0) ? true : false
          })

          await article.save()

          log['add'].push(item.uuid)
        } else {
          if (article.name != item.name) update.name = item.name
          if (article.measureName != item.measureName) update.measureName = item.measureName
          if (article.quantity != item.quantity) {
            update.quantity = item.quantity
            update.active = (item.quantity > 0) ? true : false
          } 
          if (article.price != item.price) update.price = item.price
          if (article.costPrice != item.costPrice) update.costPrice = item.costPrice
          if (article.code != item.code) update.code = item.code
          if (article.barCode != item.barCodes) update.barCode = item.barCodes
          
          if (!!Object.keys(update).length) {
            await app.get('models').Article.findOneAndUpdate({ uuid: item.uuid }, update, { setDefaultsOnInsert: true })
            log['update'].push(item.uuid)
          } 
        }
      }
    }

    for (const uuid of (await app.get('models').Article.distinct('uuid', { active: true })).filter(item => !currentArticles.includes(item))) {
      await app.get('models').Article.findOneAndUpdate({ uuid: uuid }, { active: false })
      log['hide'].push(uuid)
    }

    res.json({
      add: log['add'].length,
      update: log['update'].length,
      hide: log['hide'].length
    })
  } catch (err) {
    next(err)
  }
}

module.exports = router