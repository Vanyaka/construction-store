'uae strict'

// Пакеты
const path = require('path')
const __rootpath = require('app-root-path').path
const express = require('express')

// Модули
const other = require(path.join(__rootpath, 'routes', 'other'))

let router = express.Router()

router.route('/') // Заказы
  .get(getPage)
  .all(other.MethodNotAllowed)

/**
 * Получение страницы
 */
function getPage(req, res, next) {
  try {
    res.render('adminPanel/orders')
  } catch (err) {
    next(err)
  }
}

module.exports = router