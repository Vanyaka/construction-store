'uae strict'

// Пакеты
const path = require('path')
const __rootpath = require('app-root-path').path
const express = require('express')

// Модули
const other = require(path.join(__rootpath, 'routes', 'other'))

let router = express.Router()

router.route('/') // Пользователи
  .get(listOfAll)
  .all(other.MethodNotAllowed)

/**
 * Просмотр списка
 */
async function listOfAll(req, res, next) {
  try {
    let sortParams = {}
    let query = {}

    const data = await Promise.all([
      app.get('models').User.find(query).sort(sortParams).skip(req.prePaginationData.onPage * (req.prePaginationData.page - 1)).limit(req.prePaginationData.onPage),
      pagination(app.get('models').User, query, req.urlData, req.prePaginationData.onPage, req.prePaginationData.page)
    ])

    res.render('adminPanel/users', {
      users: data[0],
      pagination_data: data[1]
    })
  } catch (err) {
    next(err)
  }
} 


module.exports = router