'uae strict'

// Пакеты
const path = require('path')
const __rootpath = require('app-root-path').path
const express = require('express')
const fs = require('fs')
const axios = require('axios')

// Модули
const other = require(path.join(__rootpath, 'routes', 'other'))

let router = express.Router()

router.route('/') // Картинки
  .get(getPage)
  .post(upload)
  .delete(remove)
  .all(other.MethodNotAllowed)

/**
 * Получение страницы
 */
function getPage(req, res, next) {
  try {
    res.render('adminPanel/images', {
      images: fs.readdirSync(path.join(__rootpath, 'dist', 'img', 'upload')),
      path: '/img/upload/'
    })
  } catch (err) {
    next(err)
  }
}

/**
 * Загрузка картинки
 */
async function upload(req, res, next) {
  try {
    for (const key in req.body) {
      if (typeof req.body[key] === 'string') req.body[key] = req.body[key].trim()
      if (req.body[key] === '') req.body[key] = undefined
    }

    if ((!req.files || !req.files.images) && !req.body.images) return next(new HttpError(400, "Отсутствуют файлы для загрузки"))


    if (!!req.files && !!req.files.images && !Array.isArray(req.files.images)) req.files.images = [ req.files.images ]

    if (!!req.body.images) {
      for (const image of req.body.images.split(',')) {
        if (!app.get('regexes').URL.test(image.trim()) || ![ '.jpeg', '.jpg', '.png', '.svg', '.gif' ].includes(path.extname(image.trim()))) return next(new HttpError(400, 'Недопустимый формат. Используйте картинки с допустимым расширением: .jpeg, .jpg, .png, .svg, .gif'))
      }
    }

    if (!!req.files && !!req.files.images) {
      for (const image of req.files.images) {
        if (![ '.jpeg', '.jpg', '.png', '.svg', '.gif' ].includes(path.extname(image.name))) return next(new HttpError(400, 'Недопустимый формат. Используйте картинки с допустимым расширением: .jpeg, .jpg, .png, .svg, .gif'))
        if (image.size > (3 * 1024 * 1024)) return next(new HttpError(400, 'Слишком большой размер картинки. Для загрузки используйте файлы размером до 3 мб'))
      }
    }

    let promises = []
    if (!!req.body.images) {
      for (const image of req.body.images.split(',')) {
        promises.push(downloadFile(image, path.resolve(__rootpath, 'dist', 'img', 'upload', path.basename(image))))
      }
    }
    if (!!req.files && !!req.files.images) {
      for (const image of req.files.images) {
        promises.push(image.mv(path.join(__rootpath, 'dist', 'img', 'upload', image.name))) 
      }
    }

    await Promise.all(promises)

    res.redirect('back')
  } catch (err) {
    next(err)
  }
}

/**
 * Удаление картинки
 */
function remove(req, res, next) {
  try {
    if (!req.body.name) return next(new HttpError(400, 'Укажите файл для удаления'))

    fs.unlinkSync(path.join(__rootpath, 'dist', 'img', 'upload', req.body.name))

    res.json()
  } catch (err) {
    next(new HttpError(500, 'Не удалось удалить указанный файл'))
  }
}

/**
 * Загрузка файла
 * @param {String} url ссылка на файл для скачивания
 * @param {String} path путь для сохранения
 * @returns {String} путь к сохранённой картинке
 */
async function downloadFile(url, path) {
  try {
    const response = await axios({
      url: url,
      method: 'GET',
      responseType: 'stream'
    })

    await response.data.pipe(fs.createWriteStream(path))

    return path
  } catch (err) {
    throw err
  }
}

module.exports = router
