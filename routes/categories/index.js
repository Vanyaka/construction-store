'use stirct'

// Пакеты
const path = require('path')
const __rootpath = require('app-root-path').path
const express = require('express')

// Модули
const other = require(path.join(__rootpath, 'routes', 'other'))
const middlewares = require(path.join(__rootpath, 'routes', 'middlewares'))

let router = express.Router()


router.route('/') // Категории
  .get(getMainPage)
  .all(other.MethodNotAllowed)

router.route('/:id') // Категория
  .all(middlewares.checkID)
  .get(getCategoryPage)
  .all(other.MethodNotAllowed)

/**
 * Получение списка корневых категорий
 */
async function getMainPage(req, res, next) {
  try {
    res.render('categories', {
      categories: await app.get('models').Category.find({ parent: { $exists: false } })
    })
  } catch (err) {
    next(err)
  }
}

/**
 * Получение страницы категории
 */
async function getCategoryPage(req, res, next) {
  try {
    const [ categories, products ] = await Promise.all([
      app.get('models').Category.find({ parent: req.params.id }),
      app.get('models').Product.find({ categories: { $all: [ req.params.id ] } })
    ])

    if (!!categories.length) {
      res.render('categories', {
        categories: categories
      })
    } else {
      for (const product of products) {
        if (!!product['images']) product['images'] = product['images'].split(',')[0]
      }

      res.render('products', {
        products: products
      })
    }
  } catch (err) {
    next(err)
  }
}

module.exports = router
