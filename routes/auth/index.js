'use stirct'

// Пакеты
const passport = require('passport')
const path = require('path')
const __rootpath = require('app-root-path').path
const express = require('express')

// Модули
const other = require(path.join(__rootpath, 'routes', 'other'))

let router = express.Router()

router.route('/signup') // Регистрация
  .get(getSignupPage)
  .post(signup)
  .all(other.MethodNotAllowed)

router.route('/signin') // Авторизация
  .get(getSigninPage)
  .post(signin)
  .all(other.MethodNotAllowed)

router.route('/signout') // Выход
  .post(signout)
  .all(other.MethodNotAllowed)

/**
 * Получение страницы регистрации
 */
function getSignupPage(req, res, next) {
  try {
    res.render('auth/signup')
  } catch (err) {
    next(err)
  }
}

/**
 * Получение страницы авторизации
 */
function getSigninPage(req, res, next) {
  try {
    res.render('auth/signin')
  } catch (err) {
    next(err)
  }
}

/**
 * Регистрация
 */
async function signup(req, res, next) {
  for (const key in req.body) {
    if (typeof req.body[key] === 'string') req.body[key] = req.body[key].trim()
  }

  if (!req.body || req.body.email === undefined || req.body.password === undefined) return next(new HttpError(400, 'Заполните все обязательные поля'))
  if (req.body.email === '' || req.body.password === '') return next(new HttpError(400, 'Переданы некорректные данные'))
  if (!! await app.get('models').User.findOne({ email: req.body.email.toLowerCase() })) return next(new HttpError(400, 'Пользователь с таким email уже существует'))

  const user = new (app.get('models').User)({
    email: req.body.email.toLowerCase(),
    password: req.body.password
  })

  await user.save()

  req.logIn(user, (err) => {
    if (err) return next(err)

    res.redirect(req.cookies.lastURL || '/')
  })
}

/**
 * Авторизация
 */
async function signin(req, res, next) {
  try {
    for (const key in req.body) {
      if (typeof req.body[key] === 'string') req.body[key] = req.body[key].trim()
    }

    if (!req.body || req.body.email === undefined || req.body.password === undefined) return next(new HttpError(400, 'Заполните все обязательные поля'))
    if (req.body.email === '' || req.body.password === '') return next(new HttpError(400, 'Переданы некорректные данные'))
    
    passport.authenticate('local', (err, user, info) => {
      if (err) return next(err)
      if (!user) return next(info)

      req.logIn(user, (err) => {
        if (err) return next(err)

        res.redirect(req.cookies.lastURL || ((!!req.user && req.user.role == 'Администратор') ? '/admin-panel' : '/'))
      })
    })(req, res, next)
  } catch (err) {
    next (err)
  }
}

/**
 * Выход
 */
async function signout(req, res, next) {
  try {
    req.logout()
    res.redirect(req.cookies.lastURL || '/')
  } catch (err) {
    next(err)
  }
}

module.exports = router
