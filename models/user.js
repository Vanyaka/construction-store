'use strict'

// Пакеты
const crypto = require('crypto')
const mongoose = require('mongoose')

// Пользователь
module.exports = () => {
  const { Schema } = mongoose

  const schema = new Schema({
    email: { // Эл. почта
      type: String,
      required: true,
      unique: true
    },
    role: { // Роль
      type: String,
      required: true,
      enum: [ 'Пользователь', 'Администратор' ],
      default: 'Пользователь'
    },
    hashedPassword: { // Зашифрованный пароль
      type: String,
      required: true
    },
    salt: { // Соль
      type: String,
      required: true
    }
  })

  // Виртуальное поле пароля
  schema.virtual('password')
    .set(function(password) {
      this._plainPassword = password
      this.salt = Math.random() + ''
      this.hashedPassword = this.encryptPassword(password)
    })
    .get(function() {
      return this._plainPassword
    })
  
  // Шифрование пароля
  schema.methods.encryptPassword = function(password) {
    return crypto.createHmac('sha1', this.salt).update(password).digest('hex')
  }

  // Проверка пароля
  schema.methods.checkPassword = function(password) {
    return this.encryptPassword(password) === this.hashedPassword
  }

  return mongoose.model('User', schema)
}
