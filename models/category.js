'use strict'

// Пакеты
const mongoose = require('mongoose')

// Категория
module.exports = () => {
  const { Schema } = mongoose

  const schema = new Schema({
    name: { // Название
      type: String,
      required: true
    },
    parent: { // [Родительская категория]
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Category'
    },
    image: { // Картинка (URL)
      type: String
    },
    description: { // Описание
      type: String
    }
  })

  return mongoose.model('Category', schema)
}
