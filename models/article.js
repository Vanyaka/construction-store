'use strict'

// Пакеты
const mongoose = require('mongoose')

// Артикул
module.exports = () => {
  const { Schema } = mongoose

  const schema = new Schema({
    uuid: { // ID для импорта
      type: String,
      required: true,
      unique: true
    },
    name: { // Название
      type: String,
      required: true
    },
    product: { // [Товар]
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Product'
    },
    measureName: { // Наименование меры
      type: String,
      required: true
    },
    quantity: { // Количество
      type: Number,
      required: true
    },
    price: { // Розничная цена
      type: Number,
      required: true
    },
    costPrice: { // Оптовая цена
      type: Number,
      required: true
    },
    code: { // Код
      type: Number,
      required: true,
      unique: true
    },
    barCode: { // Штрих-код
      type: String,
      required: true,
      unique: true
    },
    active: { // Отображение
      type: Boolean,
      required: true,
      default: false
    }
  })

  return mongoose.model('Article', schema)
}
