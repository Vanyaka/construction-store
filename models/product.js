'use strcit'

// Пакеты
const mongoose = require('mongoose')

// Товар
module.exports = () => {
  const { Schema } = mongoose

  const schema = new Schema({
    name: { // Название
      type: String,
      required: true,
      unique: true
    },
    categories: [{ // Категории
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Category'
    }],
    images: { // Картинки (URL)
      type: String
    },
    description: { // Описание
      type: String
    },
    fullDescription: { // Подробное описание (HTML)
      type: String
    }
  })

  return mongoose.model('Product', schema)
}
