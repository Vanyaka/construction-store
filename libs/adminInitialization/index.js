'use strict'

// Инициализация администратора
module.exports = async () => {
  try {
    if(!await app.get('models').User.countDocuments({ role: 'Администратор' })) {
      const admin = new (app.get('models').User)(Object.assign(app.get('defaults').admin, {
        role: 'Администратор'
      }))

      await admin.save()

      console.log(`Создан новый администратор`)
    }
  } catch (err) {
    throw err
  }
}
