'use strict'

// Пакеты 
const qs = require('qs')

// Формирование данных для пагинации
module.exports = async (Model, query, urlData, onPage, page) => {
  try {
    const count = await Model.countDocuments(query)
    const maxPage = Math.ceil(count / onPage)

    if (page > maxPage && page != 1) throw new HttpError(404)

    const start = (page - 2 > 0) ? (page - 2) : 1
    const end = (page + 2 <= maxPage) ? (page + 2) : maxPage
    let result = []

    if (start > 1) {
      urlData.params['start'] = 1
      result.push({
        url: `${urlData.pathname}?${qs.stringify(urlData.params)}`,
        name: 'Начало',
        check: false
      })
    }

    for (let i = start; i <= end; i++) {
      urlData.params['start'] = onPage * (i - 1) + 1
      result.push({
        url: `${urlData.pathname}?${qs.stringify(urlData.params)}`,
        name: i,
        check: (i == page) ? true : false
      })
    }

    if (end < maxPage) {
      urlData.params['start'] = onPage * (maxPage - 1) + 1
      result.push({
        url: `${urlData.pathname}?${qs.stringify(urlData.params)}`,
        name: 'Конец',
        check: false
      })
    } 

    return result
  } catch (err) {
    throw err
  }
}
