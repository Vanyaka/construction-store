'use strict'

// Пакеты
const path = require('path')
const hbs = require('hbs')
const querystring = require('querystring')

// Установка и настройка шаблонизатора
module.exports = async () => {
  try {
    app.set('view engine', 'hbs')
    await hbs.registerPartials(path.join(__rootpath, 'views', 'partials'))

    // Определение класса артикула
    await hbs.registerHelper('get_article_class', function(article) {
      return (!!article.active) ? 'table-success' : 'table-secondary'
    })

    // Формирование блока пагинации
    await hbs.registerHelper('pagination', function(data) {
      if (data.length <= 1) return null

      let links = ''

      for (const item of data) {
        links += `
        <li class="page-item${(item.check) ? ' disabled': ''}"><a class="page-link" href="${item.url}">${item.name}</a></li>
        `
      }

      return new hbs.SafeString(`
      <nav>
        <ul class="pagination justify-content-center">
          ${links}
        </ul>
      </nav>
      `)
    })

    // Формирование кнопки сортировки
    await hbs.registerHelper('get_sort_button', function(urlData, param, value, title) {
      let tmpUrlData = JSON.parse(JSON.stringify(urlData))

      delete tmpUrlData.params['page']

      if (value !== undefined) {
        tmpUrlData.params[param] = value
      } else {
        delete tmpUrlData.params[param]
      }

      return new hbs.SafeString(`
      <li class="nav-item">
        <a class="nav-link${(urlData.params[param] == value) ? ' active' : ''}" href="${tmpUrlData.pathname}${(!!Object.keys(tmpUrlData.params).length) ? '?' : ''}${querystring.stringify(tmpUrlData.params)}">${title}</a>
      </li>
      `)
    })

    // Формирование вложенных опций для select
    await hbs.registerHelper('create_select_tree', (data, selfID, activeID) => {
      return new hbs.SafeString(getOptions(data, selfID, activeID))
    })

    // Формирование иерархического списка
    await hbs.registerHelper('create_hierarchy_tree', (data) => {
      return new hbs.SafeString(getItems(data))
    })

    // "Если равны"
    await hbs.registerHelper('ifEquals', function (arg1, arg2, options) {
      return (arg1 == arg2) ? options.fn(this) : options.inverse(this)
    })
  
    // Формирование списка категорий
    await hbs.registerHelper('get_categories', categories => {
      let tmp = ''

      for (const category of categories) {
        tmp += `<a href="/admin-panel/categories/${category.id}" target="_blank">${category.name}</a>, `
      }

      return new hbs.SafeString(tmp.replace(/\, $/, ''))
    })
  } catch (err) {
    throw err
  }
}

// Построение иерархического списка
function getItems(data) {
  let items = ''

  for (const item of data) {
    items += `<a href="/admin-panel/categories/${item._id}" class="list-group-item list-group-item-action"><span data-feather="chevron-right"></span><span>${item.name}</span></a>`

    if (!!item.children.length) items += getItems(item.children)
  }

  return new hbs.SafeString(`
  <div class="list-group list-group-flush">
    ${items}
  </div>
  `)
}

// Рекурсивное построение дерева опций
function getOptions(data, selfID, activeID, symbol = '&nbsp;&nbsp;&nbsp;', level = 0) {
  let result = ''

  for (const item of data) {
    if (item._id != selfID) {
      result += `
      <option value=${item._id}${(activeID == item._id) ? ' selected' : ''}>${symbol.repeat(level)}${item.name}</option>
      `
  
      result += getOptions(item.children, selfID, activeID, symbol, level + 1)
    }
  } 

  return result
}
