'use strict'

// Пакеты
const path = require('path')
const mongoose = require('mongoose')
const session = require('express-session')
const mStore = require('connect-mongo')(session)

// Подключение и настройка Mongoose
module.exports = async () => {
  try {
    await mongoose.connect(app.get('mongodb').uri, app.get('mongodb').options)
    app.use(session(Object.assign(app.get('mongodb').session, {
      store: new mStore({ mongooseConnection: mongoose.connection })
    })))

    app.set('models', {
      Category: require(path.join(__rootpath, 'models', 'category'))(),
      Product: require(path.join(__rootpath, 'models', 'product'))(),
      Article: require(path.join(__rootpath, 'models', 'article'))(),
      User: require(path.join(__rootpath, 'models', 'user'))()
    })

    return true
  } catch (err) {
    throw err
  }
}
