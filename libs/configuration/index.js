'use strict'

// Пакеты
const path = require('path')

// Запись параметров конфигурации во внутренне хранилище (для быстрого доступа)
module.exports = async () => {
  try {
    const config = require(path.join(__rootpath, 'config'))

    for (let param in config) {
      app.set(param, config[param])
    }

    return true
  } catch (err) {
    throw err
  }
}
