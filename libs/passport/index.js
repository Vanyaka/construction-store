'use strict'

// Пакеты
const passport = require('passport')
const localStrategy = require('passport-local').Strategy

// Настройка и подключение passport.js
module.exports = () => {
  // Сериализация (сервер -> клиент)
  passport.serializeUser((user, done) => done(null, user._id))

  // Десериализация (сервер <- клиент)
  passport.deserializeUser(async (id, done) => {
    try {
      done(null, await app.get('models').User.findById(id))
    } catch (err) {
      done(err)
    }
  })

  // Локальная стратегия
  passport.use(new localStrategy({
      usernameField: 'email',
      passwordField: 'password'
    },
    async (username, password, done) => {
      try {
        const user = await app.get('models').User.findOne({ email: username.toLowerCase() })

        if (!user || !user.checkPassword(password)) return done(null, false, new HttpError(403, 'Пользователь с такими данными не найден'))

        done(null, user)
      } catch (err) {
        done(err)
      }
    }
  ))

  app.use(passport.initialize())
  app.use(passport.session())
}
