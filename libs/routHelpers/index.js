'use strict'

/**
 * Рекурсивное формирование дерева отношений "родитель-ребёнок"
 * @description на основе полей "parent" и "_id" выстраивает древовидную структуру
 * @param {Object[]} data - массив объектов (выборка из БД)
 * @param {String} parentCode - код родителя
 * @returns массив вложенных (через поле "children") объектов
 */
exports.getChildrenTree = (data, parentCode) => {
  let result = []

  for (let i = 0; i < data.length; i++) {
    if (data[i].parent == parentCode) {
      let value = JSON.parse(JSON.stringify(data[i]))

      value['children'] = this.getChildrenTree(data, value._id)

      result.push(value)
      data.splice(i--, 1)
    } 
  }

  return result
}
