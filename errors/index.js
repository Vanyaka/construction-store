'use strict'

// Обработчик ошибок
module.exports = () => {
  app.use((err, req, res, next) => {
    if (res.headersSent) return next(err) // Если ответ уже начал записываться, то передаём ошибку в стандартный обработчик

    if (app.get('env') == 'development') console.log(err)

    if (!(err instanceof HttpError)) err = new HttpError() // Обработка исключений

    switch (req.accepts([ 'html', 'json' ])) {
      case 'html': {
        res.status(err.status).render('error', {
          status: err.status,
          msg: err.message
        })

        break
      }
      case 'json': {
        res.status(err.status).json({
          status: err.status,
          msg: err.message
        })

        break
      }
      default: {
        err = new HttpError(400)
        res.status(err.status).send(err.message)
      } 
    }
  })
}
