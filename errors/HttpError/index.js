'use strict'

// Пакеты
const util = require('util')
const http = require('http')

/**
 * Кастомный объект ошибки (HTTP)
 * 
 * @constructor
 * @param {number} status - статус ошибки
 * @param {string} message - сообщение ошибки
 */
function HttpError(status = 500, message) {
  Error.apply(this, arguments)
  Error.captureStackTrace(this, HttpError)

  this.status = status
  this.message = message || app.get('messages').statuses[status] || http.STATUS_CODES[status]
}

util.inherits(HttpError, Error)
HttpError.prototype.name = 'HttpError'

module.exports = HttpError
