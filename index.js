'use strict'

// Пакеты
const path = require('path')
const express = require('express')
const bParser = require('body-parser')
const cParser = require('cookie-parser')
const fileUpload = require('express-fileupload')

// Модули
const HttpError = require(path.join(__dirname, 'errors', 'HttpError'))
const pagination = require(path.join(__dirname, 'libs', 'pagination'))
const configuration = require(path.join(__dirname, 'libs', 'configuration'))
const mongooseConnect = require(path.join(__dirname, 'libs', 'mongooseConnect'))
const passport = require(path.join(__dirname, 'libs', 'passport'))
const adminInitialization = require(path.join(__dirname, 'libs', 'adminInitialization'))
const setViewEngine = require(path.join(__dirname, 'libs', 'setViewEngine'))
const routHelpers = require(path.join(__dirname, 'libs', 'routHelpers'))
const middlewares = {
  toLocal: require(path.join(__dirname, 'middlewares', 'toLocal')),
  urlParse: require(path.join(__dirname, 'middlewares', 'urlParse')),
  prePagination: require(path.join(__dirname, 'middlewares', 'prePagination')),
  lastURL: require(path.join(__dirname, 'middlewares', 'lastURL'))
}
const routes = require(path.join(__dirname, 'routes'))
const errors = require(path.join(__dirname, 'errors'));

(async () => {
  try {
    // Установка глобальных значений
    global.__rootpath = __dirname // Абсолютный путь до корня проекта
    global.app = express()
    global.HttpError = HttpError
    global.pagination = pagination
    global.routHelpers = routHelpers

    // Настройка 
    await configuration()
    await mongooseConnect()
    passport()
    await adminInitialization()
    await setViewEngine()
    app.use(bParser.urlencoded({ extended: true }))
    app.use(bParser.json())
    app.use(cParser())
    app.use(fileUpload())
    app.use(express.static(path.join(__rootpath, 'dist')))

    // Промежуточные обработчики
    app.use(middlewares.urlParse)
    app.use(middlewares.prePagination)
    app.use(middlewares.lastURL)
    app.use(middlewares.toLocal)

    routes() // Роутинг

    errors() // Обработка ошибок

    app.listen(app.get('port'), app.get('host'), () => console.log(`"${app.get('name')}" стартовал на http://${app.get('host')}:${app.get('port')}`))
  } catch (err) {
    throw err
  }
})()
