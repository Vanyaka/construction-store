'use strict'

// Пакеты
const url = require('url')
const qs = require('qs')

// Парсинг URL
module.exports = (req, res, next) => {
  try {
    const data = url.parse(req.url)

    let urlData = {
      protocol: req.protocol, // Протокол
      host: req.get('x-forwarded-host') || req.get('host'), // Имя хоста + порт
      hostname: req.get('x-forwarded-server') || req.hostname, // Имя хоста
      params: qs.parse(data.query), // Объект со структурированными параметрами
      pathname: data.pathname, // Путь
      clientIP: req.get('x-forwarded-for') // IP с которого пришёл запрос
    }

    // Предварительная обработка параметров
    for (const param in urlData.params) {
      if (param == 'start' && (isNaN(Number(urlData.params['start'])) || urlData.params['start'] <= 0)) {
        delete urlData.params[param]
      } else if (urlData.params[param] === '') {
        urlData.params[param] = true
      } 
    }

    res.locals.urlData = JSON.parse(JSON.stringify(urlData))
    req.urlData = JSON.parse(JSON.stringify(urlData))

    next()
  } catch (err) {
    next(err)
  }
}
