'use strict'

// Подготовка некоторых данных для пагинации
module.exports = (req, res, next) => {
  try {
    let data = {}

    data['onPage'] = (Number(req.cookies.onPage) || app.get('defaults').onPage)
    data['page'] = Math.ceil((Number(req.urlData.params['start']) || 1) / data['onPage'])

    req.prePaginationData = data

    next()
  } catch(err) {
    next(err)
  }
}
