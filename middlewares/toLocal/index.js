'use strict'

// Пакеты
const path = require('path')
const fs = require('fs')

// "Локализация" данных
module.exports = async (req, res, next) => {
  try {
    res.locals.user = (req.isAuthenticated()) ? req.user : null

    res.locals.styles = fs.readdirSync(path.join(__rootpath, 'dist', 'css'))
    res.locals.scripts = fs.readdirSync(path.join(__rootpath, 'dist', 'js'))

    res.locals.display = (req.cookies.display || app.get('defaults').display)

    res.locals.origin = `${req.urlData.protocol}://${req.urlData.host}`

    res.locals.allCategories = await app.get('models').Category.find()

    next()
  } catch (err) {
    next(err)
  }
}
