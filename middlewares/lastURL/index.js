'use string'

// Запоминание последней посещённой страницы, не связанной с авторизацией
module.exports = (req, res, next) => {
  try {
    if (!!req.accepts('html') && !/\/auth\//.test(req.url)) res.cookie('lastURL', req.url, { path: '/', httpOnly: true })
    
    next()
  } catch (err) {
    next(err)
  }
}