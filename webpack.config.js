'use strict'

// Пакеты
const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

// Конфигурации Webpack
module.exports = {
  entry: path.join(__dirname, 'src', 'index.js'),
  output: {
    filename: path.join('js', 'script.[contenthash].js'),
    path: path.join(__dirname, 'dist')
  },
  module: {
    rules: [
      { // babel
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              [
                '@babel/preset-env',
                {
                  targets: {
                    browsers: '> 3%'
                  }
                }
              ]
            ]
          }
        }
      }, 
      { // scss
        test: /\.s[ac]ss$/i,
        use: [ 'style-loader', MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader', 'sass-loader' ]
      },
    ]
  },
  plugins: [
    new CleanWebpackPlugin({ cleanOnceBeforeBuildPatterns: ['css/*', 'js/*'] }),
    new MiniCssExtractPlugin({
      filename: path.join('css', 'style.[contenthash].css')
    })
  ]
}
